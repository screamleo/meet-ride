//
//  URRentService.h
//  uRide
//
//  Created by Anton Ivashyna on 7/8/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "URObject.h"

@class URCity, URScooter;

@interface URRentService : URObject

@property (nonatomic, retain) NSNumber * addressLalitude;
@property (nonatomic, retain) NSNumber * addressLongitude;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * number;
@property (nonatomic, retain) NSNumber * rating;
@property (nonatomic, retain) NSData * logo;
@property (nonatomic, retain) NSData * rentPhoto1;
@property (nonatomic, retain) NSData * rentPhoto2;
@property (nonatomic, retain) NSData * rentPhoto3;
@property (nonatomic, retain) NSData * rentPhoto4;
@property (nonatomic, retain) NSNumber * rentPriceForGiorno;
@property (nonatomic, retain) NSNumber * rentPriceForVino;
@property (nonatomic, retain) NSNumber * rentPriceForJoker;
@property (nonatomic, retain) URCity *city;
@property (nonatomic, retain) NSSet *scooters;

@property (assign, nonatomic) double distanceFromUser;

@end

@interface URRentService (CoreDataGeneratedAccessors)

- (void)addScootersObject:(URScooter *)value;
- (void)removeScootersObject:(URScooter *)value;
- (void)addScooters:(NSSet *)values;
- (void)removeScooters:(NSSet *)values;

@end
