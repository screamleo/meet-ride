//
//  URTimePickerViewController.h
//  uRide
//
//  Created by Anton Ivashyna on 7/8/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol URTimePickerPopoverDelegate;

@interface URTimePickerPopover : UIViewController

@property (weak, nonatomic) id <URTimePickerPopoverDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIDatePicker *timePicker;

- (IBAction) rentTimeChangedAction:(UIDatePicker *)sender;

@end

@protocol URTimePickerPopoverDelegate <NSObject>

@required
- (void) setTimeFromPopover: (NSDate*) date;

@end

