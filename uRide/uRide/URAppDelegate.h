//
//  AppDelegate.h
//  uRide
//
//  Created by Leo Lashkevich on 6/23/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface URAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

