//
//  URMeetTableViewController.m
//  uRide
//
//  Created by Anton Ivashyna on 7/18/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import "URMeetTableViewController.h"
#import "URMapViewController.h"

#import "URMainManager.h"
#import <Parse/Parse.h>

#import "URMainCell.h"

#import "URMeet.h"
#import "URUser.h"

#import "DGActivityIndicatorView.h"

@interface URMeetTableViewController ()

@property (strong, nonatomic) NSMutableArray* participants;

@property (weak, nonatomic) IBOutlet UIImageView *meetPhotoImageView;

@property (weak, nonatomic) IBOutlet UIButton *takePartInMeetButton;

@property (strong, nonatomic) DGActivityIndicatorView* activityIndicator;

- (IBAction)actionAddParticipant:(UIButton *)sender;

@end

@implementation URMeetTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupActivityIndicator];
    [self.activityIndicator startAnimating];
    
    URUser* user = [URUser currentUser];
    
    [self configureHeader];
    
    NSMutableArray* allIDs = [[NSMutableArray alloc] init];
    
    self.participants = [NSMutableArray array];
    
    [[URMainManager sharedManager] getParticipantsForMeet:self.meet onSuccess:^(NSArray *participants, NSArray* participanstIDs) {
        
        [self.participants addObjectsFromArray:participants];
        [allIDs addObjectsFromArray:participanstIDs];
        
        NSMutableArray* indexPaths = [NSMutableArray array];
        
        for (int i = 0; i < [self.participants count]; i ++) {
            NSIndexPath* indexPath = [NSIndexPath indexPathForItem:i inSection:0];
            [indexPaths addObject:indexPath];
        }
        
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView endUpdates];
        
        [self.activityIndicator stopAnimating];
        [self.activityIndicator removeFromSuperview];
        
        for (NSString* userID in participanstIDs) {
            
            NSString* userIDStrind = [NSString stringWithFormat:@"%@", user.userID];
            NSString* newString = [NSString stringWithFormat:@"%@", userID];
            
            if ([userIDStrind isEqualToString:newString]) {
                
                self.takePartInMeetButton.enabled = NO;
                [self.takePartInMeetButton setTitle:@"Вы уже подписаны" forState:UIControlStateDisabled];
            }
        }
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setupActivityIndicator {
    
    NSArray *activityTypes = @[@(DGActivityIndicatorAnimationTypeRotatingTrigons)];
    NSArray *sizes = @[@(40.0f)];
    
    for (int i = 0; i < activityTypes.count; i++) {
        self.activityIndicator = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)[activityTypes[i] integerValue] tintColor:[UIColor whiteColor] size:[sizes[i] floatValue]];
        
        CGFloat width = self.view.bounds.size.width / 2.0f;
        CGFloat height = self.view.bounds.size.height / 2.0f;
        self.activityIndicator.frame = CGRectMake(width /2 , height /2  - 70, width, height);
        
        [self.view addSubview:self.activityIndicator];
    }
    
}

- (void) configureHeader {
    
    self.navigationItem.title = self.meet.name;
    
    UIBarButtonItem* backButton = [[UIBarButtonItem alloc] initWithTitle:@"Назад" style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(actionBack:)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    
    UIImage* mapIcon = [UIImage imageNamed:@"map_icon.png"];
    CGRect mapIconRect = CGRectMake(0, 0, 20, 20);
    UIButton *mapIconButton = [[UIButton alloc] initWithFrame:mapIconRect];
    [mapIconButton setBackgroundImage:mapIcon forState:UIControlStateNormal];
    [mapIconButton addTarget:self action:@selector(actionOpenMap:)
            forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* openMapItem = [[UIBarButtonItem alloc] initWithCustomView:mapIconButton];
    
    self.navigationItem.rightBarButtonItems = @[openMapItem];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.meetPhotoImageView.image = self.meet.photo;
    self.meetPhotoImageView.layer.masksToBounds = YES;
    
}

#pragma mark - Actions

- (void)actionOpenMap:(UIButton *)sender {
    
    URMapViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"URMapViewController"];
    vc.objectsToDisplay = @[self.meet.place];
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (IBAction)actionAddParticipant:(UIButton *)sender {
    
    [sender setTitle:@"Мы тебя ждем" forState:UIControlStateNormal];
    
    URUser* user = [URUser currentUser];
    
    [[URMainManager sharedManager] addNewParticipantWithID:user.userID toMeet:self.meet onSuccess:^(bool saved) {
        
        [self.tableView reloadData];
        
    } onFailure:^(NSError *error) {
        
    }];
    
}

- (void) actionBack: (UIBarButtonItem*) item {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataDource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.participants count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* participantCell = @"participantCell";
    
    URMainCell *cell = [tableView dequeueReusableCellWithIdentifier:participantCell];
    
    if (!cell) {
        cell = [[URMainCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:participantCell];
    }
    
    [self configureCell:cell forIndexPath:indexPath];
    
    return cell;
}

- (void) configureCell: (URMainCell*) cell forIndexPath: (NSIndexPath*) indexPath {
    
    PFObject* user = self.participants[indexPath.row];
    
    PFFile* imafeFile = [user valueForKey:@"photo"];
    [imafeFile getDataInBackgroundWithBlock:^(NSData *PF_NULLABLE_S data, NSError *PF_NULLABLE_S error) {
        cell.photoImageView.image = [UIImage imageWithData:data];
        cell.photoImageView.layer.cornerRadius =  cell.photoImageView.frame.size.width / 2;
        cell.photoImageView.layer.masksToBounds = YES;
    }];
    
    cell.firstLineLabel.text = [NSString stringWithFormat:@"%@ %@", [user valueForKey:@"firstName"], [user valueForKey:@"lastName"]];
    cell.secondLineLabel.text = [user valueForKey:@"scooter"];
    
}

#pragma mark - UITableViewDelegate

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64.f;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
