//
//  URScooterRentFormTableViewController.m
//  uRide
//
//  Created by Anton Ivashyna on 7/8/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//
//
//
//*********************************************************
//
//  Here user can rent scooter.
//
//*********************************************************

#import "URScooterRentFormTableViewController.h"

#import "URScooterRentCell.h"

#import "URDatePickerPopover.h"
#import "URTimePickerPopover.h"

#import <MessageUI/MessageUI.h>

@interface URScooterRentFormTableViewController () <UICollectionViewDataSource, UICollectionViewDelegate,
                                                    WYPopoverControllerDelegate, UITextFieldDelegate,
                                                    URDatePickerPopover, URTimePickerPopoverDelegate,
                                                    MFMailComposeViewControllerDelegate>

typedef enum : NSUInteger {
    
    URTextFieldTagName,
    URTextFieldTagLastName,
    URTextFieldTagPhoneNumber,
    URTextFieldTagTime,
    URTextFieldTagDate,
    
} URTextFieldTag;

@property (strong, nonatomic) NSArray* scooterPhotosData;

@property (strong, nonatomic) WYPopoverController* pickerPopoverController;
@property (strong, nonatomic) URDatePickerPopover* controllerForDatePickerView;
@property (strong, nonatomic) URTimePickerPopover* controllerForTimePickerView;

@property (strong, nonatomic) URScooterRentCell* cell;

@property (strong, nonatomic) NSString* email;

@end

@implementation URScooterRentFormTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setPopover];
    
    self.navigationItem.title = self.scooter.name;
    
    NSDictionary* emailsDictionary = @{@"GogoRide"      : @"hello@scooterrent.com.ua",
                                       @"SidScooters"   : @"khizhnyakivan@gmail.com",
                                       @"ScooterRide"   : @"megawina@live.ru",
                                       @"LvovRent"      : @"pantin837@mail.ru",
                                       @"Foxtrans"      : @"Fox_trans@mail.ru",
                                       @"HelloScooter"  : @"Vdorogoj@yandex.ru"};
    
    self.email = [emailsDictionary objectForKey:self.rentService.name];
    
    NSLog(@"%@", self.email);
    

    self.scooterPhotosData = @[self.scooter.photo1, self.scooter.photo2,
                               self.scooter.photo3, self.scooter.photo4];
    
    UIBarButtonItem* saveItem = [[UIBarButtonItem alloc] initWithTitle:@"Далее"
                                                                 style:UIBarButtonItemStylePlain
                                                                target:self
                                                                action:@selector(nextButtonAction:)];
    
    self.navigationItem.rightBarButtonItem = saveItem;
    
    self.controllerForDatePickerView = [self.storyboard instantiateViewControllerWithIdentifier:@"datePickerStoryboardID"];
    self.controllerForDatePickerView.delegate = self;
    self.controllerForTimePickerView = [self.storyboard instantiateViewControllerWithIdentifier:@"timePickerStoryboardID"];
    self.controllerForTimePickerView.delegate = self;
            
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)changeTimeAction:(UIButton *)sender {
    
    self.pickerPopoverController = [[WYPopoverController alloc] initWithContentViewController:self.controllerForTimePickerView];
    
    self.pickerPopoverController.delegate = self;
    
    [self.pickerPopoverController setPopoverContentSize:CGSizeMake(250.f, 200.f)];
    
    [self.pickerPopoverController presentPopoverFromRect:sender.bounds
                                                  inView:sender
                                permittedArrowDirections:WYPopoverArrowDirectionDown
                                                animated:YES];
    
}

- (IBAction)changeDateAction:(UIButton *)sender {
    
    self.pickerPopoverController = [[WYPopoverController alloc] initWithContentViewController:self.controllerForDatePickerView];
    
    self.pickerPopoverController.delegate = self;
        
    [self.pickerPopoverController setPopoverContentSize:CGSizeMake(250.f, 200.f)];
    
    [self.pickerPopoverController presentPopoverFromRect:sender.bounds
                                            inView:sender
                          permittedArrowDirections:WYPopoverArrowDirectionDown
                                          animated:YES];
    
}

- (void) nextButtonAction: (id) sender {
    NSLog(@"nextButtonAction");
    
    NSString* messageBody = [NSString stringWithFormat:
     @"Привет. Меня зовут %@ %@. Мне нужен мопед %@ на %@ %@ числа. Свяжитесь со мной по телефону %@",
                                              self.cell.nameTextField.text, self.cell.lastNameTextField.text, self.scooter.name,
                               self.cell.timeTextField.text,
                             self.cell.dateTextField.text,      self.cell.phoneNumberTextField.text];
    
    if([MFMailComposeViewController canSendMail]) {
        NSLog(@"send");
        
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        
        mailCont.mailComposeDelegate = self;
        
        [mailCont setSubject:@"Rent Scooter"];
        
        [mailCont setToRecipients:@[self.email]];
        
        [mailCont setMessageBody:messageBody isHTML:NO];
        
        [self presentViewController:mailCont animated:YES completion:nil];
    }
    
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 4;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* scooterPhotoID = @"scooterPhotoID";
    
    UICollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:scooterPhotoID forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[UICollectionViewCell alloc] initWithFrame:CGRectMake(10, 10, 150, 150)];
    }
    
    [cell.layer setBorderWidth:2.0f];
    [cell.layer setBorderColor:[UIColor colorWithRed:0/255.f green:180/255.f blue:156/255.f alpha:1].CGColor];
    
    UIImageView* scooterPhotoImageView = (UIImageView*)[cell viewWithTag:200];
    
    NSData* scooterPhotoData = self.scooterPhotosData[indexPath.row];
    
    UIImage* scooterPhoto = [UIImage imageWithData:scooterPhotoData];
    
    scooterPhotoImageView.image = scooterPhoto;
    
    return cell;
    
}

#pragma mark - UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(URScooterRentCell*) tableView: (UITableView*) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* contactFormCellID = @"contactFormCellID";
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    URScooterRentCell* cell = [tableView dequeueReusableCellWithIdentifier:contactFormCellID forIndexPath:indexPath];
    
    if (!cell) {
        cell =  [[URScooterRentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:contactFormCellID];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    
    self.cell = cell;
    
    return cell;
}

-(void)configureCell:(URScooterRentCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    cell.nameTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"firstName"];
    cell.lastNameTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastName"];
    
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
        
    if (textField.tag == URTextFieldTagTime ||
        textField.tag == URTextFieldTagDate) {
        return NO;
    } else {
        return YES;
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - Popover methods

- (void) setPopover {
    
    UIColor *greenColor = [UIColor colorWithRed:0.f/255.f green:183.f/255.f blue:157.f/255.f alpha:1];
    
    [WYPopoverController setDefaultTheme:[WYPopoverTheme theme]];
    
    WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
    
    [popoverAppearance setOuterCornerRadius:4];
    [popoverAppearance setOuterShadowBlurRadius:0];
    [popoverAppearance setOuterShadowColor:[UIColor clearColor]];
    [popoverAppearance setOuterShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
    [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setBorderWidth:8];
    [popoverAppearance setArrowHeight:10];
    [popoverAppearance setArrowBase:20];
    
    [popoverAppearance setInnerCornerRadius:4];
    [popoverAppearance setInnerShadowBlurRadius:0];
    [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
    [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setFillTopColor:greenColor];
    [popoverAppearance setFillBottomColor:greenColor];
    [popoverAppearance setOuterStrokeColor:greenColor];
    [popoverAppearance setInnerStrokeColor:greenColor];
    
    UINavigationBar* navBarInPopoverAppearance = [UINavigationBar appearanceWhenContainedIn:[UINavigationController class], [WYPopoverController class], nil];
    [navBarInPopoverAppearance setTitleTextAttributes: @{
                                                         NSForegroundColorAttributeName : [UIColor whiteColor],
                                                         NSShadowAttributeName : [UIColor clearColor],
                                                         NSShadowAttributeName : [NSValue valueWithUIOffset:UIOffsetMake(0, -1)]}];
    
}

#pragma mark - URDatePickerViewDelegate

- (void) setDateFromPopover:(NSDate *)date {
    
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc]init];
    
    [dateFormater setDateFormat:@"dd-M-yyyy"];
    
    self.cell.dateTextField.text = [dateFormater stringFromDate:date];
        
}

#pragma mark - URTimePickerViewDelegate

- (void) setTimeFromPopover:(NSDate *)date {
    
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc]init];
    
    [dateFormater setDateFormat:@"HH:mm"];
    
    self.cell.timeTextField.text = [dateFormater stringFromDate:date];
    
}


@end
