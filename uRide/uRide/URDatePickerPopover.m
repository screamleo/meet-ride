//
//  URDatePickerViewController.m
//  uRide
//
//  Created by Anton Ivashyna on 7/8/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import "URDatePickerPopover.h"

@implementation URDatePickerPopover

- (IBAction)rentDateChangedAction:(UIDatePicker *)sender {
    
    [self.delegate setDateFromPopover:self.datePicker.date];
    
}


@end
