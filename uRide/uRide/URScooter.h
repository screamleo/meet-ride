//
//  URScooter.h
//  uRide
//
//  Created by Anton Ivashyna on 7/8/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "URObject.h"

@class URRentService;

@interface URScooter : URObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSData * photo1;
@property (nonatomic, retain) NSData * photo2;
@property (nonatomic, retain) NSData * photo3;
@property (nonatomic, retain) NSData * photo4;
@property (nonatomic, retain) NSSet *rentService;
@end

@interface URScooter (CoreDataGeneratedAccessors)

- (void)addRentServiceObject:(URRentService *)value;
- (void)removeRentServiceObject:(URRentService *)value;
- (void)addRentService:(NSSet *)values;
- (void)removeRentService:(NSSet *)values;

@end
