//
//  URDistanceCalculator.h
//  uRide
//
//  Created by Anton Ivashyna on 7/17/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@class PFGeoPoint;
@class MKMapView;
@class CLLocationManager, CLLocation;


@interface URDistanceCalculator : NSObject

@property (strong, nonatomic) CLLocationManager* locationManager;
@property (strong, nonatomic) CLLocation* location;

- (void) calculateDistanceToDestinationPoint: (PFGeoPoint*) destinationLocation
                                   onSuccess: (void(^)(CLLocationDistance distance)) success
                                   onFailure: (void(^)(NSError* error)) failure;

@end



