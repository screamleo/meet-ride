//
//  URCity.m
//  uRide
//
//  Created by Anton Ivashyna on 7/6/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import "URCity.h"
#import "URRentService.h"

@implementation URCity

@dynamic name;
@dynamic photo;
@dynamic rentServices;

@end
