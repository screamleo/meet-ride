//
//  URRentServicesTableViewController.h
//  uRide
//
//  Created by Anton Ivashyna on 7/4/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "URCity.h"

@class MKMapView;
@class CLLocationManager, CLLocation;

@interface URRentServicesTableViewController : UITableViewController

@property (strong, nonatomic) URCity* city;

@property (strong, nonatomic) CLLocationManager* locationManager;
@property (strong, nonatomic) CLLocation* location;

@end
