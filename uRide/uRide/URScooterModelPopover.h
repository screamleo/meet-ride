//
//  URScooterModelTableViewController.h
//  uRide
//
//  Created by Anton Ivashyna on 7/16/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol URScooterModelPopoverDelegate;

@interface URScooterModelPopover : UITableViewController

@property (weak, nonatomic) id <URScooterModelPopoverDelegate> delegate;

@end


@protocol URScooterModelPopoverDelegate <NSObject>

- (void) setScooterModel: (NSString*) model;

@end