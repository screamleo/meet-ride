//
//  URCitiesTableVIewController.m
//  uRide
//
//  Created by Anton Ivashyna on 7/3/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//
//*********************************************************
//
//  This view controller is created to work with objects in Core Data. Here we generate
//      core data model (cities, rent services, scooters for them).
//
//*********************************************************

#import "URCitiesTableViewController.h"
#import "URRentServicesTableViewController.h"

#import "URCoreDataManager.h"

#import "URCityCell.h"

#import "URCity.h"
#import "URScooter.h"
#import "URRentService.h"


@interface URCitiesTableViewController ()

@property (strong, nonatomic) NSArray* citiesNames;
@property (strong, nonatomic) NSArray* citiesPhotos;
@property (strong, nonatomic) URCity* city;

@end

@implementation URCitiesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSInteger timesLaunched = [[NSUserDefaults standardUserDefaults]
                               integerForKey:@"firstTimeLaunch"];         // we have to check if user open this view controller at first.if Yes - create core data
    timesLaunched++;                                                                                            //      objects (cities, rent services, scooters)
    
    [[NSUserDefaults standardUserDefaults] setInteger:timesLaunched forKey:@"firstTimeLaunch"];
    
    if ((long)[[NSUserDefaults standardUserDefaults] integerForKey:@"firstTimeLaunch"] == 2) {                  // bool value in this case doesn`t work
    
        [self createCities];
        [self createScooters];
        [self addScootersToRentServices];
        [self createRentPricesForScootersInRentServices];
    }

}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];
    [super viewWillAppear:animated];
}

#pragma mark - Creating of objects

- (void) createCities {
    
    [self deleteAllObjects];
    
    // create cities and rent services in it
    
    //Kiev
    
    NSArray* kievRentServicesNames              = @[@"GogoRide", @"SidScooters", @"ScooterRide"];
    NSArray* kievRentServicesaddressLongitude   = @[@30.482380, @30.537420, @30.507595];
    NSArray* kievRentServicesaddressLalitude    = @[@50.435549, @50.422064, @50.455778];
    NSArray* kievRentServicesNumbers            = @[@"063 104 21 00", @"093 740 50 40", @"093 971 90 74"];
    NSArray* kievRentServicesLogos              = @[@"GogoRide.jpg", @"SidScooters.jpg", @"ScooterRide.jpg"];
    
    //Lviv
    
    NSArray* lvivRentServicesNames              = @[@"LvovRent"];
    NSArray* lvivRentServicesaddressLongitude   = @[@24.026017];
    NSArray* lvivRentServicesaddressLalitude    = @[@49.843290];
    NSArray* lvivRentServicesNumbers            = @[@"093 810 71 29"];
    NSArray* lvivRentServicesLogos              = @[@"LvovRent.jpg"];
    
    //Odessa
    
    NSArray* odessaRentServicesNames            = @[@"Foxtrans", @"HelloScooter"];
    NSArray* odessaRentServicesaddressLongitude = @[@30.718654, @30.738005];
    NSArray* odessaRentServicesaddressLalitude  = @[@46.454532, @46.485881];
    NSArray* odessaRentServicesNumbers          = @[@"063 884 83 74", @"093 241 11 24"];
    NSArray* odessaRentServicesLogos            = @[@"Foxtrans.jpg", @"HelloScooter.jpg"];
    
    
    URCity* kievCity    = [self createCityWithName: @"Киев"];
    URCity* lvivCity    = [self createCityWithName: @"Львов"];
    URCity* odessaCity  = [self createCityWithName: @"Одесса"];
    
    kievCity.photo      = UIImagePNGRepresentation([UIImage imageNamed:@"city_Kiyv"]);
    lvivCity.photo      = UIImagePNGRepresentation([UIImage imageNamed:@"city_Lviv"]);
    odessaCity.photo    = UIImagePNGRepresentation([UIImage imageNamed:@"city_Odessa"]);
    
    // creating of arrays, which contains photos of scooters for rent service
    
    NSArray* cities                           = @[kievCity, lvivCity, odessaCity];
    NSArray* cityRentServicesNames            = @[kievRentServicesNames, lvivRentServicesNames, odessaRentServicesNames];
    NSArray* cityRentServicesAddressLongitude = @[kievRentServicesaddressLongitude, lvivRentServicesaddressLongitude, odessaRentServicesaddressLongitude];
    NSArray* cityRentServicesAddressLatitude  = @[kievRentServicesaddressLalitude, lvivRentServicesaddressLalitude, odessaRentServicesaddressLalitude];
    NSArray* cityRentServicesNumbers          = @[kievRentServicesNumbers, lvivRentServicesNumbers, odessaRentServicesNumbers];
    NSArray* cityRentServicesLogosNames       = @[kievRentServicesLogos, lvivRentServicesLogos, odessaRentServicesLogos];
    
    
    for (int i = 0; i < [cities count]; i ++) {
        
        URCity* city = cities[i];
        
        NSArray* namesForRentServicesInCity             = cityRentServicesNames[i];
        NSArray* addressLongitudesForRentServicesInCity = cityRentServicesAddressLongitude[i];
        NSArray* addressLatitudesForRentServicesInCity  = cityRentServicesAddressLatitude[i];
        NSArray* numbersForRentServicesInCity           = cityRentServicesNumbers[i];
        NSArray* logosForRentServicesInCity             = cityRentServicesLogosNames[i];
        
        for (int i = 0; i < [namesForRentServicesInCity count]; i ++) {
            
            NSString* logoName = logosForRentServicesInCity[i];                     // to get logo we need take it`s name from array
            
            UIImage* logoImage = [UIImage imageNamed:logoName];
            
            NSData* logoData = UIImageJPEGRepresentation(logoImage, 0.1f);
            
            // to add an array of photos for UICollectionView
            
            NSArray* scooterPhotos = [[NSArray alloc] init];
            
            NSMutableArray* mutArr = [NSMutableArray arrayWithArray:scooterPhotos];
            
            for (int i = 0; i < 4; i++) {
                
                NSString* rentServiceName = logoName;
                
                NSString *logoNameWithoutJPG = [rentServiceName substringToIndex:[rentServiceName length] - 4];
                
                NSString* imageForRentServiceName = [NSString stringWithFormat:@"%@%d.jpg",logoNameWithoutJPG, i+1];
                
                UIImage* imageOfScooter = [UIImage imageNamed:imageForRentServiceName];
                
                NSData* photoData = UIImageJPEGRepresentation(imageOfScooter, 0.1f);
                
                [mutArr addObject:photoData];
                
            }
            
            scooterPhotos = mutArr;
            
            [self createRentServiceForCity:city
                       withRentServiceName:namesForRentServicesInCity[i]
                          addressLongitude: addressLongitudesForRentServicesInCity[i]
                           addressLatitude:addressLatitudesForRentServicesInCity[i]
                               phoneNumber:numbersForRentServicesInCity[i]
                                      logo:logoData
                             scooterPhotos:scooterPhotos];
            
        }
        
    }
    
}

- (URCity*) createCityWithName: (NSString*) name {
    
    URCity* city = [NSEntityDescription insertNewObjectForEntityForName:@"URCity"
                                                 inManagedObjectContext:self.managedObjectContext];
    city.name = name;
    
    return city;
}

- (void) createRentServiceForCity: (URCity*) city
              withRentServiceName: (NSString*) rentServiceName
                 addressLongitude: (id) addressLongitude
                  addressLatitude: (id) addressLatitude
                      phoneNumber: (NSString*) phoneNumber
                             logo: (NSData* ) logo
                    scooterPhotos: (NSArray*) photos {
    
    URRentService* rentService = [self createRentServiceWithName:rentServiceName];
    
    rentService.addressLongitude = addressLongitude;
    rentService.addressLalitude  = addressLatitude;
    rentService.number           = phoneNumber;
    rentService.logo             = logo;
    
    rentService.rentPhoto1 = [photos objectAtIndex:0];
    rentService.rentPhoto2 = [photos objectAtIndex:1];
    rentService.rentPhoto3 = [photos objectAtIndex:2];
    rentService.rentPhoto4 = [photos objectAtIndex:3];
    
    
    [city addRentServicesObject:rentService];
    
    [self.managedObjectContext save:nil];

}

- (URRentService*) createRentServiceWithName: (NSString*) name {
    
    URRentService* rentService = [NSEntityDescription insertNewObjectForEntityForName:@"URRentService"
                                                               inManagedObjectContext:self.managedObjectContext];
    rentService.name = name;
    
    return rentService;

}

#pragma mark - Scooters

- (void) createScooters{
    
    NSArray* scooterNames = @[@"Honda Giorno", @"Yamaha Vino", @"Honda Joker"];
    
    for (int i = 0; i < [scooterNames count]; i++) {
        
        URScooter* scooter = [NSEntityDescription insertNewObjectForEntityForName:@"URScooter"
                                                           inManagedObjectContext:self.managedObjectContext];
        
        scooter.name = scooterNames[i];
        
        NSArray* photosArray = [[NSArray alloc] init];
        
        NSMutableArray* arr = [NSMutableArray arrayWithArray:photosArray];
        
        for (int i = 0; i < 4; i ++) {
            NSString* photoName = [NSString stringWithFormat:@"%@%d.png", scooter.name, i + 1];
            UIImage* scooterPhoto = [UIImage imageNamed:photoName];
            [arr addObject:scooterPhoto];
        }
        
        photosArray = arr;
        
        [self addPhotos:photosArray toScooter:scooter];
        
        [self.managedObjectContext save:nil];
    }
    
}

- (void) addPhotos: (NSArray*) photosArray toScooter: (URScooter*) scooter {
    
    scooter.photo1 = UIImagePNGRepresentation(photosArray[0]);
    scooter.photo2 = UIImagePNGRepresentation(photosArray[1]);
    scooter.photo3 = UIImagePNGRepresentation(photosArray[2]);
    scooter.photo4 = UIImagePNGRepresentation(photosArray[3]);
    
    [self.managedObjectContext save:nil];
    
}

- (void) addScootersToRentServices {
    
    // these methods hepls us a lot to set only those scooters, that rent service have. For example, SidScooters have only Honda Giorno
    
    NSPredicate* predicateToGetServicesForOnlyHondaGiorno =
    [NSPredicate predicateWithFormat:
     @"name == %@ OR name == %@ OR name == %@ OR name == %@ OR name == %@ OR name == %@",
     @"GogoRide", @"SidScooters", @"ScooterRide", @"LvovRent", @"Foxtrans", @"HelloScooter"];
    
    NSPredicate* predicateToGetServicesForOnlyYamahaVino =
    [NSPredicate predicateWithFormat:
     @"name == %@ OR name == %@ OR name == %@ OR name == %@ OR name == %@",
     @"GogoRide", @"ScooterRide", @"LvovRent", @"Foxtrans", @"HelloScooter"];
    
    NSPredicate* predicateToGetServicesForOnlyHondaJoker =
    [NSPredicate predicateWithFormat:
     @"name == %@",
     @"GogoRide"];
    
    
    NSArray* rentServicesWithHondaGiorno = [self getObjectsForEntity:@"URRentService"
                                                       withPredicate:predicateToGetServicesForOnlyHondaGiorno];
    
    NSArray* rentServicesWithYamahaVino = [self getObjectsForEntity:@"URRentService"
                                                      withPredicate:predicateToGetServicesForOnlyYamahaVino];
    
    NSArray* rentServicesWithHondaJoker = [self getObjectsForEntity:@"URRentService"
                                                      withPredicate:predicateToGetServicesForOnlyHondaJoker];
    
    // fetch scooters to add them to services
    
    NSArray* scooters = [self getObjectsForEntity:@"URScooter" withPredicate:nil];
    
    for (id object in rentServicesWithHondaGiorno) {
        URRentService* rentService = (URRentService*) object;
        [rentService addScootersObject:(URScooter*)(scooters[0])];
    }
    
    for (id object in rentServicesWithYamahaVino) {
        URRentService* rentService = (URRentService*) object;
        [rentService addScootersObject:(URScooter*)(scooters[1])];
    }
    
    for (id object in rentServicesWithHondaJoker) {
        URRentService* rentService = (URRentService*) object;
        [rentService addScootersObject:(URScooter*)(scooters[2])];
    }

    [self.managedObjectContext save:nil];
    
}

#pragma mark - Setting of rent prices

- (void) createRentPricesForScootersInRentServices {
    
    NSArray* rentServices = [self getObjectsForEntity:@"URRentService" withPredicate:nil];
    
    for (id object in rentServices) {
        URRentService* rentService = (URRentService*) object;
        [self setRentPriceForScootersInRentService:rentService];
    }
    [self.managedObjectContext save:nil];
    
}

- (void) setRentPriceForScootersInRentService: (URRentService*) rentService {
    
    if ([rentService.name
                isEqualToString:@"GogoRide"]) {
        
        rentService.rentPriceForGiorno  = @(400);
        rentService.rentPriceForVino    = @(450);
        rentService.rentPriceForJoker   = @(550);
        
    } else if ([rentService.name
                isEqualToString:@"SidScooters"]) {
        
        rentService.rentPriceForGiorno  = @(400);
        
    } else if ([rentService.name
                isEqualToString:@"ScooterRide"]) {
        
        rentService.rentPriceForGiorno  = @(400);
        rentService.rentPriceForVino    = @(400);
        
    } else if ([rentService.name
                isEqualToString:@"LvovRent"]) {
        
        rentService.rentPriceForGiorno  = @(450);
        rentService.rentPriceForVino    = @(450);
        
    } else if ([rentService.name
                isEqualToString:@"Foxtrans"]) {
        
        rentService.rentPriceForGiorno  = @(250);
        rentService.rentPriceForVino    = @(250);
        
    } else if ([rentService.name
                isEqualToString:@"HelloScooter"]) {
        
        rentService.rentPriceForGiorno  = @(250);
        rentService.rentPriceForVino    = @(250);
    }
    
}

#pragma mark - Other

- (NSArray*) getObjectsForEntity: (NSString*) entityName withPredicate: (NSPredicate*) predicate {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    return fetchedObjects;
    
}

- (void) deleteAllObjects {
    
    NSArray* allObjects = [self getObjectsForEntity:@"URObject" withPredicate:nil];
    
    for (id object in allObjects) {
        [self.managedObjectContext deleteObject:object];
    }
    
    [self.managedObjectContext save:nil];
    
}

#pragma mark - Core Data and creating of objects


- (NSManagedObjectContext*) managedObjectContext {
    
    if (!_managedObjectContext) {
        _managedObjectContext = [[URCoreDataManager sharedManager] managedObjectContext];
    }
    return _managedObjectContext;
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self getObjectsForEntity:@"URCity" withPredicate:nil].count;
}

- (URCityCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* cityCellID = @"cityCellID";
    
    URCityCell *cell = [tableView dequeueReusableCellWithIdentifier:cityCellID forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[URCityCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cityCellID];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

-(void)configureCell:(URCityCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSArray* cities = [self getObjectsForEntity:@"URCity" withPredicate:nil];
    
    URCity* city = cities[indexPath.row];
    
    cell.cityPictureImageView.image = [UIImage imageWithData:city.photo];
    cell.cityNameLabel.text = city.name;
    
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray* cities = [self getObjectsForEntity:@"URCity" withPredicate:nil];
    
    self.city = cities[indexPath.row];
    
    URRentServicesTableViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"URRentServicesTableViewController"];
    vc.city = self.city;
    [self.navigationController pushViewController:vc animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

@end
