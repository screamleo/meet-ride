//
//  URScooter.m
//  uRide
//
//  Created by Anton Ivashyna on 7/8/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import "URScooter.h"
#import "URRentService.h"


@implementation URScooter

@dynamic name;
@dynamic photo1;
@dynamic photo2;
@dynamic photo3;
@dynamic photo4;
@dynamic rentService;

@end
