//
//  CSDHelpPageVC.h
//  CyberSecurityDigest
//
//  Created by Leo Lashkevich on 6/20/15.
//  Copyright (c) 2015 Leo Lashkevich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSDHelpPageVC : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageViewMini;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *imageFile;


@end
