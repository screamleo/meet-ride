//
//  UserProfileTableViewController.h
//  uRide
//
//  Created by Anton Ivashyna on 7/15/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface URUserProfileTableViewController : UITableViewController

@end
