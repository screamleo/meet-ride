//
//  URTimePickerViewController.m
//  uRide
//
//  Created by Anton Ivashyna on 7/8/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import "URTimePickerPopover.h"

@implementation URTimePickerPopover

- (IBAction) rentTimeChangedAction:(UIDatePicker *)sender {
    
    [self.delegate setTimeFromPopover:self.timePicker.date];
    
}

@end
