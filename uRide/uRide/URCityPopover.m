//
//  URCityPopoverTableViewController.m
//  uRide
//
//  Created by Anton Ivashyna on 7/16/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//
//*********************************************************
//
//  TableViewController for popover, when user have to select
//      city
//
//*********************************************************

#import "URCityPopover.h"

#import "WYPopoverController.h"

@interface URCityPopover () <WYPopoverControllerDelegate>

@property (strong, nonatomic) NSArray* cities;
@property (strong, nonatomic) NSIndexPath* lastIndexPath;

@end

@implementation URCityPopover

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    
    self.cities = @[@"Киев", @"Одесса", @"Львов"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSouce

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.cities count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* cityCell = @"cityCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cityCell];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cityCell];
    }
    
    if ([indexPath compare:self.lastIndexPath] == NSOrderedSame) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.textLabel.text = self.cities[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    self.lastIndexPath = indexPath;
    
    [tableView reloadData];
    
    [self.delegate setCityForUser:self.cities[indexPath.row]];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end
