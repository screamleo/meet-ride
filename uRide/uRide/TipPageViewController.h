//
//  TipPageViewController.h
//  CyberSecurityDigest
//
//  Created by Leo Lashkevich on 6/20/15.
//  Copyright (c) 2015 Leo Lashkevich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TipPageViewController : UIViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate>
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;
@property (strong, nonatomic) IBOutlet UIButton *tipButton;
- (IBAction)tipButtonPressed:(id)sender;

@end
