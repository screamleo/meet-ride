//
//  URRentServicesTableViewController.m
//  uRide
//
//  Created by Anton Ivashyna on 7/4/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//
//*********************************************************
//
//  This view controller is created to work with rent services objects from
//      core data for city, which was chosen in previous controller.
//      We use MapKit, CLLocationManager, etc. to calculate distances
//      to rent services from users current location.
//
//*********************************************************

#import "URRentServicesTableViewController.h"
#import "URRentServiceTableViewController.h"

#import "URMainCell.h"

#import "URRentService.h"
#import "URScooter.h"

#import "URDistanceCalculator.h"

#import <CoreLocation/CoreLocation.h>
#import <Parse/Parse.h>

@interface URRentServicesTableViewController () <CLLocationManagerDelegate>

@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;

@property (strong, nonatomic) NSMutableArray* distances;

@end

@implementation URRentServicesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.distances = [NSMutableArray array];
    
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor colorWithRed:88/255 green:197/255 blue:168/255 alpha:1];
    
    self.locationManager = [[CLLocationManager alloc ] init];
    self.locationManager.delegate = self;
    
    [self.locationManager requestAlwaysAuthorization];
    [self.locationManager startUpdatingLocation];

}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Core Data

- (NSArray*) getObjectsForEntity: (NSString*) entityName withPredicate: (NSPredicate*) predicate {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    return fetchedObjects;
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    NSMutableArray *rentServices = [NSMutableArray arrayWithArray:[self.city.rentServices allObjects]];
    return [rentServices count];
    
}

- (URMainCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* rentServiceCellID = @"rentServiceCellID";
    
    URMainCell *cell = [tableView dequeueReusableCellWithIdentifier:rentServiceCellID forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[URMainCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:rentServiceCellID];
    }
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
    
}

-(void)configureCell:(URMainCell *)cell atIndexPath:(NSIndexPath *)indexPath {

    NSMutableArray *rentServices = [NSMutableArray arrayWithArray:[self.city.rentServices allObjects]];
    
    URRentService* rentService = rentServices[indexPath.row];
    
    URDistanceCalculator* calc = [[URDistanceCalculator alloc] init];
    
    PFGeoPoint* rentPoint = [PFGeoPoint geoPointWithLatitude:[rentService.addressLalitude doubleValue]
                                                   longitude:[rentService.addressLongitude doubleValue]];
    
    [calc calculateDistanceToDestinationPoint:rentPoint onSuccess:^(CLLocationDistance distance) {
        
        NSString* disctanceString = [NSString stringWithFormat:@"%.1f км", distance];
        cell.diclosureTextLabel.text = disctanceString;
        [self.distances addObject:disctanceString];
        
    } onFailure:^(NSError *error) {
        
    }];
    
    cell.firstLineLabel.text = rentService.name;
    cell.secondLineLabel.text = [self getScootersNamesForRentService:rentService];
    
    UIImageView* imageView = (UIImageView*)[cell viewWithTag:300];
    
    imageView.image = [UIImage imageWithData:rentService.logo];
    [imageView.layer setCornerRadius:imageView.frame.size.width/2];
    imageView.clipsToBounds = YES;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64.f;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSMutableArray* rentServices = [NSMutableArray arrayWithArray:[self.city.rentServices allObjects]];
    
    URRentService* rentService = rentServices[indexPath.row];
    
    URRentServiceTableViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"rentServiceID"];
    
    vc.distanceFromUser = self.distances[indexPath.row];
    vc.rentService = rentService;
    
    [self.navigationController pushViewController:vc  animated:YES];
    
}

#pragma mark - Other

- (NSString*) getScootersNamesForRentService: (URRentService*) rentService {
    
    NSMutableArray* scootersOfRentServices = [NSMutableArray arrayWithArray:[rentService.scooters allObjects]];
    
    NSString* scootersString = @"";
    
    for (id object in scootersOfRentServices) {
        
        URScooter* scooter = (URScooter* )(object);
        
        // devide in 2 words to get the last object and display it in "secondLineLabel"
        
        NSArray* words = [scooter.name componentsSeparatedByString:@" "];
        
        NSString* scooterName = [words lastObject];
        
        scootersString = [scootersString stringByAppendingString:[NSString stringWithFormat:@"%@ ", scooterName]];
        
    }
    
    return scootersString;
    
}

@end
