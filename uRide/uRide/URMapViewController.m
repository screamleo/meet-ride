//
//  URMapViewController.m
//  uRide
//
//  Created by Anton Ivashyna on 7/17/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import "URMapViewController.h"

#import <CoreLocation/CoreLocation.h>

#import "URMapAnnotation.h"

#import "UIView+URView_MKAnnotationView.h"

@interface URMapViewController () <MKMapViewDelegate, CLLocationManagerDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) MKDirections *directions;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView* activityIndicator;      //it become active when the кoad is rendering

@property (strong, nonatomic) NSMutableArray* annotationsArray;

@end

@implementation URMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.activityIndicator.hidden = YES;
    
    self.annotationsArray = [NSMutableArray array];
    
    [self configureLocationManager];
    
    UIBarButtonItem* backButton = [[UIBarButtonItem alloc] initWithTitle:@"Вернуться" style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(actionBack:)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    UIBarButtonItem* searchButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch
                                                                                  target:self
                                                                                  action:@selector(actionShowAll:)];
    if (self.comeFromNewMeet == YES) {
        
        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"mapOpenedFirstTime"]) {
            [[[UIAlertView alloc] initWithTitle:nil
                                        message:@"Добавить встречу - долгое нажатие на нужное место на карте. Также ты можешь проложить к этому месту маршрут, нажав на пункт назначения и выбрав кнопку 'Маршрут'"
                                       delegate:self
                              cancelButtonTitle:@"Понял"
                              otherButtonTitles:nil, nil] show];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"mapOpenedFirstTime"];
        }
        
        
        UIBarButtonItem* saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Сохранить"
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(actionSave:)];
        
        self.navigationItem.rightBarButtonItems = @[saveButton, searchButton];
        
        UILongPressGestureRecognizer* longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                                       action:@selector(actionPutMeetPlace:)];
        longPressGesture.delegate = self;
        longPressGesture.minimumPressDuration = 1.f;
        [self.view addGestureRecognizer:longPressGesture];
        
    } else {
        
        self.navigationItem.title = @"Мы здесь";                                // we set it when come from meet view controller
        
        for (PFGeoPoint* point in self.objectsToDisplay) {
            
            CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(point.latitude, point.longitude);
            URMapAnnotation* annotation = [[URMapAnnotation alloc] init];
            annotation.coordinate = coordinate;
            
            [self.mapView addAnnotations:@[annotation]];
            [self actionShowAll:nil];
        
        self.navigationItem.rightBarButtonItems = @[searchButton];
        
        }
    }
    
}

- (void) configureLocationManager {
    
    self.locationManager = [[CLLocationManager alloc ] init];
    self.locationManager.delegate = self;
    
    [self.locationManager requestAlwaysAuthorization];
    [self.locationManager startUpdatingLocation];
    self.mapView.showsUserLocation = YES;
    
    [self.locationManager startUpdatingLocation];
    
}

#pragma mark - Actions

- (void) actionSave: (UIBarButtonItem*) item {
    
    URMapAnnotation* meetAnnotation = [self.annotationsArray lastObject];
    CLLocationCoordinate2D meetCoordinate = meetAnnotation.coordinate;
    
    PFGeoPoint* point = [PFGeoPoint geoPointWithLatitude:meetCoordinate.latitude
                                               longitude:meetCoordinate.longitude];
    
    [self.delegate setPointForNewMeet:point];
        
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void) actionPutMeetPlace: (UILongPressGestureRecognizer*) sender {
        
    if ([self.annotationsArray count] > 1) {                                        // in this case we check user to insert only 1 pin
        [self.mapView removeAnnotations:self.annotationsArray];
    }
    
    CGPoint pointInMainView = [sender locationInView:self.view];                    // user have to make longGesture to put pin on map
    
    URMapAnnotation* annotation = [[URMapAnnotation alloc] init];
    
    annotation.title        = [self.meetName length] == 0 ? @"Еще нет названия" : self.meetName;
    annotation.subtitle     = [self.dateString length] == 1 ? @"Еще нет даты" : self.dateString;
    annotation.coordinate   = [self.mapView convertPoint:pointInMainView toCoordinateFromView:self.view];
    
    [self.annotationsArray addObject:annotation];
    [self.mapView addAnnotation:annotation];

}

- (void) actionShowAll: (UIBarButtonItem*) item {
    
    MKMapRect zoomRect = MKMapRectNull;                                             // this method provides ability for user to see all pins in his display
    
    static double delta = 20000;
    
    for (id <MKAnnotation> annotation in self.mapView.annotations) {
        
        CLLocationCoordinate2D location = annotation.coordinate;
        MKMapPoint center = MKMapPointForCoordinate(location);
        MKMapRect annotationRect = MKMapRectMake(center.x - delta, center.y - delta, delta * 2, delta * 2);
        
        zoomRect = MKMapRectUnion(zoomRect, annotationRect);
    }
    zoomRect = [self.mapView mapRectThatFits:zoomRect];
    
    [self.mapView setVisibleMapRect:zoomRect edgePadding:UIEdgeInsetsMake(50, 50, 50, 50) animated:YES];
    
}

- (void) actionCancel: (id) sender {
    [self.mapView removeAnnotations:self.annotationsArray];
    
}

- (void) actionDirection: (UIButton *) sender {
    
    self.activityIndicator.hidden = NO;                                             // when rightCalloutAccessoryView tapped - user could see how to ride to it
    [self.activityIndicator startAnimating];
    
    MKAnnotationView *annotationView = [sender superAnnotationView];
    
    if (!annotationView) {
        return;
    }
    
    if ([self.directions isCalculating]) {
        [self.directions cancel];
    }
    
    CLLocationCoordinate2D coordinate = annotationView.annotation.coordinate;
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    
    request.source = [MKMapItem mapItemForCurrentLocation];
    
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate
                                                   addressDictionary:nil];
    
    MKMapItem *destination = [[MKMapItem alloc] initWithPlacemark:placemark];
    
    request.destination = destination;
    
    request.transportType = MKDirectionsTransportTypeAutomobile;
    request.requestsAlternateRoutes = NO;
    
    self.directions = [[MKDirections alloc] initWithRequest:request];
    
    [self.directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        
        if (error) {
            
            [[[UIAlertView alloc] initWithTitle:@"Уппс" message:@"Что-то пошло не так. Проверьте качество интернета и пропробуйте еще раз." delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil] show];
            
        } else if ([response.routes count] == 0) {
            
            [[[UIAlertView alloc] initWithTitle:@"Стоп" message:@"Похоже, к пункту назначения нет дорог." delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil] show];
            
        } else {
            
            [self.mapView removeOverlays:[self.mapView overlays]];
            
            NSMutableArray *array = [NSMutableArray array];
            
            for (MKRoute *route in response.routes) {
                [array addObject:route.polyline];
            }
            [self.mapView addOverlays:array level:MKOverlayLevelAboveRoads];
        }
    }];
}

- (void) actionBack: (UIBarButtonItem*) item {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
        
    static NSString* identifier = @"identifier";
    
    MKPinAnnotationView* pin =(MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    
    if (!pin) {
        
        pin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        
        pin.pinColor = MKPinAnnotationColorPurple;
        pin.animatesDrop = NO;
        pin.canShowCallout = YES;
        pin.draggable = YES;
        
        NSLog(@"viewForAnnotation");
        UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];         // to allow user delete pin, if create new meet and make mistake while tap the map
        UIImageView* cancelView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        cancelView.image = [UIImage imageNamed:@"cancel.png"];
        [cancelButton addSubview:cancelView];
        
        [cancelButton addTarget:self action:@selector(actionCancel:)
                    forControlEvents:UIControlEventTouchUpInside];
        pin.leftCalloutAccessoryView = cancelButton;
        
        
        UIButton *directionButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];      // begin road overlays rendering
        UIImageView* navigateView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        navigateView.image = [UIImage imageNamed:@"navigation.png"];
        [directionButton addSubview:navigateView];
        
        [directionButton addTarget:self action:@selector(actionDirection:)
                    forControlEvents:UIControlEventTouchUpInside];
        pin.rightCalloutAccessoryView = directionButton;
        
    } else {
        pin.annotation = annotation;
    }
    
    return pin;
    
}

- (MKOverlayRenderer*) mapView: (MKMapView *) mapView rendererForOverlay:(id<MKOverlay>)overlay {   // method for rendering of road from users current location to destination
    
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        
        MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
        renderer.lineWidth = 2.0f;
        renderer.strokeColor =[UIColor colorWithRed:1.0f green:0.3f blue:0.8f alpha:0.8f];
        [self.activityIndicator stopAnimating];
        return renderer;
    }
    
    return nil;
}


#pragma mark - Location manager

-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    [self.locationManager requestAlwaysAuthorization];
    [self.locationManager startUpdatingLocation];
    
    self.location = locations.lastObject;
}

-(void)statLocationManager {
    
    self.locationManager = [[CLLocationManager alloc] init];
    
    self.locationManager.delegate        = self;
    self.locationManager.distanceFilter  = kCLDistanceFilterNone;       //whenever we move
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [self.locationManager startUpdatingLocation];
    [self.locationManager requestWhenInUseAuthorization];               // Add This Line
    
}

@end
