//
//  URMeetingsTableViewController.h
//  uRide
//
//  Created by Anton Ivashyna on 7/16/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MKMapView;
@class CLLocationManager, CLLocation;

@interface URMeetsTableViewController : UITableViewController

@property (strong, nonatomic) CLLocationManager* locationManager;
@property (strong, nonatomic) CLLocation* location;

@end


