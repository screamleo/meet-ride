//
//  URCityPopoverTableViewController.h
//  uRide
//
//  Created by Anton Ivashyna on 7/16/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol URCityPopoverDelegate;

@interface URCityPopover : UITableViewController

@property (weak, nonatomic) id <URCityPopoverDelegate> delegate;

@end


@protocol URCityPopoverDelegate <NSObject>

- (void) setCityForUser: (NSString*) city;

@end