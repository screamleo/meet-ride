//
//  CSDHelpPageVC.m
//  CyberSecurityDigest
//
//  Created by Leo Lashkevich on 6/20/15.
//  Copyright (c) 2015 Leo Lashkevich. All rights reserved.
//

#import "CSDHelpPageVC.h"

@interface CSDHelpPageVC ()

@end

@implementation CSDHelpPageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.backgroundImageView.image = [UIImage imageNamed:self.imageFile];
//    self.backgroundImageViewMini.image = [UIImage imageNamed:self.imageFile];
    self.titleLabel.text = self.titleText;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
