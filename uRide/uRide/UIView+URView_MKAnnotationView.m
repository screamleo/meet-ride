//
//  UIView+URView_MKAnnotationView.m
//  uRide
//
//  Created by Anton Ivashyna on 7/17/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import "UIView+URView_MKAnnotationView.h"
#import <MapKit/MKAnnotationView.h>


@implementation UIView (MKAnnotationView)

- (MKAnnotationView *) superAnnotationView
{
    if ([self isKindOfClass:[MKAnnotationView class]]) {
        return (MKAnnotationView *)(self);
    }
    
    if (!self.superview) {
        return nil;
    }
    
    return [self.superview superAnnotationView];
}

@end
