//
//  URMeetTableViewController.h
//  uRide
//
//  Created by Anton Ivashyna on 7/18/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>

@class URMeet;

@interface URMeetTableViewController : UITableViewController

@property (strong, nonatomic) URMeet* meet;

@end
