//
//  URCitiesTableVIewController.h
//  uRide
//
//  Created by Anton Ivashyna on 7/3/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface URCitiesTableViewController : UITableViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
