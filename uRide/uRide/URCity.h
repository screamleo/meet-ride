//
//  URCity.h
//  uRide
//
//  Created by Anton Ivashyna on 7/6/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "URObject.h"

@class URRentService;

@interface URCity : URObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSData * photo;
@property (nonatomic, retain) NSSet *rentServices;
@end

@interface URCity (CoreDataGeneratedAccessors)

- (void)addRentServicesObject:(URRentService *)value;
- (void)removeRentServicesObject:(URRentService *)value;
- (void)addRentServices:(NSSet *)values;
- (void)removeRentServices:(NSSet *)values;

@end
