//
//  URScooterModelTableViewController.m
//  uRide
//
//  Created by Anton Ivashyna on 7/16/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//
//*********************************************************
//
//  TableViewController to allow user change scooter model.
//
//*********************************************************

#import "URScooterModelPopover.h"

#import "WYPopoverController.h"

@interface URScooterModelPopover () <WYPopoverControllerDelegate>

@property (strong, nonatomic) NSArray* models;
@property (strong, nonatomic) NSIndexPath* lastIndexPath;

@end

@implementation URScooterModelPopover

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    
    self.models = @[@"Honda Giorno", @"Honda Scoopy", @"Honda Joker", @"Yamaha Vino", @"Suzuki Verde"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSouce

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.models count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* modelCell = @"modelCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:modelCell];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:modelCell];
    }
    
    if ([indexPath compare:self.lastIndexPath] == NSOrderedSame) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.textLabel.text = self.models[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate 

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //[tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    
    self.lastIndexPath = indexPath;
    
    [tableView reloadData];
    
    [self.delegate setScooterModel:self.models[indexPath.row]];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
