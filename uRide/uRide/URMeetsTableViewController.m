//
//  URMeetingsTableViewController.m
//  uRide
//
//  Created by Anton Ivashyna on 7/16/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//
// TODO - removeMeetings if they ended more than 1 day ago

#import "URMeetsTableViewController.h"
#import "URMainManager.h"
#import "URMainCell.h"
#import "NSDate+Helper.h"

#import "URMeet.h"
#import "URUser.h"

#import <Parse/Parse.h>
#import <CoreLocation/CoreLocation.h>

#import "URDistanceCalculator.h"

#import "WYPopoverController.h"
#import "URCityPopover.h"
#import "URMeetTableViewController.h"

#import "DGActivityIndicatorView.h"

@interface URMeetsTableViewController () <CLLocationManagerDelegate, URCityPopoverDelegate, WYPopoverControllerDelegate>

@property (strong, nonatomic) URUser* user;
@property (strong, nonatomic) URCity* city;

@property (strong, nonatomic) NSMutableArray* allMeets;
@property (strong, nonatomic) NSMutableArray* distances;

@property (strong, nonatomic) WYPopoverController* citiesPopover;

@property (strong, nonatomic) DGActivityIndicatorView* activityIndicator;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *popoverItem;

@end

@implementation URMeetsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupActivityIndicator];
    [self.activityIndicator startAnimating];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    
    self.allMeets  = [NSMutableArray array];
    self.distances = [NSMutableArray array];
    
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"firstTimeLaunch"] == 0) {
        [self setPopover];
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"firstTimeLaunch"];
        NSLog(@"firstTimeLaunch - %ld",[[NSUserDefaults standardUserDefaults] integerForKey:@"firstTimeLaunch"]);
                                                                    // to set default city for current user ()if we do not this,
                                                                    //  if user create new meet - app should crash (objects is not NSNull)
        [self showCityPopover];
        
    } else {
        [self getMeetsForCity];
    }

}

- (void) setupActivityIndicator {
    
    NSArray *activityTypes = @[@(DGActivityIndicatorAnimationTypeRotatingTrigons)];
    NSArray *sizes = @[@(40.0f)];
    
    for (int i = 0; i < activityTypes.count; i++) {
        self.activityIndicator = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)[activityTypes[i] integerValue] tintColor:[UIColor whiteColor] size:[sizes[i] floatValue]];
        
        CGFloat width = self.view.bounds.size.width / 2.0f;
        CGFloat height = self.view.bounds.size.height / 2.0f;
        self.activityIndicator.frame = CGRectMake(width /2 , height /2  - 70, width, height);
        
        [self.view addSubview:self.activityIndicator];
    }
    
}

- (void) getMeetsForCity {
    
    [[URMainManager sharedManager] getMeetsForCity:self.city onSuccess:^(NSArray *meetings) {
        
        for (PFObject* object in meetings) {
            URMeet* meet = [[URMeet alloc] initWithObject:object];
            
            URDistanceCalculator* calc = [[URDistanceCalculator alloc] init];
            [calc calculateDistanceToDestinationPoint:meet.place onSuccess:^(CLLocationDistance distance) {
                
                meet.distance = distance;
                [self.allMeets addObject:meet];
                
                NSLog(@"meet name - %@", meet.name);
                
            } onFailure:^(NSError *error) {
                
            }];
        }
        
        NSSortDescriptor* sortByDate = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES];
        [self.allMeets sortUsingDescriptors:[NSArray arrayWithObject:sortByDate]];
        
        NSMutableArray* indexPaths = [NSMutableArray array];
        
        for (int i = 0; i < [meetings count]; i ++) {
            NSIndexPath* indexPath = [NSIndexPath indexPathForItem:i inSection:0];
            [indexPaths addObject:indexPath];
        }
        
        [self.activityIndicator stopAnimating];
        [self.activityIndicator removeFromSuperview];
        
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView endUpdates];
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.allMeets count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* meetCell = @"meetCell";
    
    URMainCell *cell = [tableView dequeueReusableCellWithIdentifier:meetCell forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[URMainCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:meetCell];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void) configureCell: (URMainCell*) cell atIndexPath: (NSIndexPath*) indexPath {
    
    URMeet* meet = self.allMeets[indexPath.row];
    
    cell.photoImageView.image = meet.photo;
    cell.photoImageView.layer.cornerRadius = cell.photoImageView.frame.size.width / 2;
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm dd/MM"];
    NSString* dateString = [dateFormatter stringFromDate:meet.date];
    
    cell.firstLineLabel.text     = meet.name;
    cell.secondLineLabel.text    = dateString;
    cell.diclosureTextLabel.text = [NSString stringWithFormat:@"%.1f км", meet.distance];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
        return 64.f;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    URMeet* meet = self.allMeets[indexPath.row];
    
    NSLog(@"%@", meet.meetID);
    
    URMeetTableViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"URMeetTableViewController"];
    vc.meet = meet;
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark - Popover and city delegate

- (void) showCityPopover {
    
    URCityPopover* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"URCityPopover"];
    vc.delegate = self;
    self.citiesPopover = [[WYPopoverController alloc] initWithContentViewController:vc];
    [self.citiesPopover setPopoverContentSize:CGSizeMake(250.f, 230.f)];
    self.citiesPopover.delegate = self;
    
    [self.citiesPopover presentPopoverFromBarButtonItem:self.popoverItem
                               permittedArrowDirections:WYPopoverArrowDirectionDown
                                               animated:YES];
    
}

-(void) setCityForUser: (NSString *)city {
    self.user = [URUser currentUser];
    self.user.city = city;
    [self.citiesPopover dismissPopoverAnimated:YES];
    
    [self getMeetsForCity];

}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)popoverController {
    if (!self.user.city) {
        return NO;
    } else {
        return YES;
    }
    
}

- (void) setPopover {
    
    UIColor *greenColor = [UIColor colorWithRed:0.f/255.f green:183.f/255.f blue:157.f/255.f alpha:1];
    
    [WYPopoverController setDefaultTheme:[WYPopoverTheme theme]];
    
    WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
    
    [popoverAppearance setOuterCornerRadius:4];
    [popoverAppearance setOuterShadowBlurRadius:0];
    [popoverAppearance setOuterShadowColor:[UIColor clearColor]];
    [popoverAppearance setOuterShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
    [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setBorderWidth:8];
    [popoverAppearance setArrowHeight:10];
    [popoverAppearance setArrowBase:20];
    
    [popoverAppearance setInnerCornerRadius:4];
    [popoverAppearance setInnerShadowBlurRadius:0];
    [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
    [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setFillTopColor:greenColor];
    [popoverAppearance setFillBottomColor:greenColor];
    [popoverAppearance setOuterStrokeColor:greenColor];
    [popoverAppearance setInnerStrokeColor:greenColor];
    
    UINavigationBar* navBarInPopoverAppearance = [UINavigationBar appearanceWhenContainedIn:[UINavigationController class], [WYPopoverController class], nil];
    [navBarInPopoverAppearance setTitleTextAttributes: @{
                                                         NSForegroundColorAttributeName : [UIColor whiteColor],
                                                         NSShadowAttributeName : [UIColor clearColor],
                                                         NSShadowAttributeName : [NSValue valueWithUIOffset:UIOffsetMake(0, -1)]}];
    
}

@end
