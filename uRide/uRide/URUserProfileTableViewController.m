//
//  UserProfileTableViewController.m
//  uRide
//
//  Created by Anton Ivashyna on 7/15/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//
//*********************************************************
//
//  Here we show info to current user. He can change it also.
//  Here is very interesting method for phone textfield
//  validation.
//
//*********************************************************

#import "URUserProfileTableViewController.h"

#import <Parse/Parse.h>
#import "URMainManager.h"

#import "WYPopoverController.h"
#import "URScooterModelPopover.h"
#import "URNewMeetTableViewController.h"

#import "DGActivityIndicatorView.h"

#import "URMainCell.h"

#import "URUser.h"
#import "URMeet.h"

#import "NSDate+Helper.h"

@interface URUserProfileTableViewController () <WYPopoverControllerDelegate, UITextFieldDelegate,
                                                URScooterModelPopoverDelegate>

@property (strong, nonatomic) URUser* currentUser;
@property (strong, nonatomic) NSMutableArray* meetings;

@property (strong, nonatomic) WYPopoverController*  popover;
@property (strong, nonatomic) URScooterModelPopover* scooterModelTabelViewController;

@property (assign, nonatomic) BOOL editState;

@property (weak, nonatomic) IBOutlet UIImageView *greenCircleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *userPhotoImageView;

@property (weak, nonatomic) IBOutlet UILabel  *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel  *userPhoneNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel  *userScooterLabel;
@property (weak, nonatomic) IBOutlet UILabel  *userCreateMeetsLabel;
@property (weak, nonatomic) IBOutlet UILabel  *userFutureMeetsLabel;
@property (weak, nonatomic) IBOutlet UIButton *createNewMeetButton;

@property (weak, nonatomic) IBOutlet UIView *viewForProfileInfo;

@property (weak, nonatomic) IBOutlet UITextField *userPhoneNumberTextField;
@property (weak, nonatomic) IBOutlet UIButton *userChooseScooterButton;

@property (strong, nonatomic) DGActivityIndicatorView* activityIndicator;


@end

@implementation URUserProfileTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupActivityIndicator];
    [self.activityIndicator startAnimating];
    
    self.currentUser = [URUser currentUser];
    
    self.meetings = [NSMutableArray array];
    
    [self setPopover];
    [self configureScreenNoEdit];
    
    self.scooterModelTabelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"URScooterModelTableViewController"];
    self.scooterModelTabelViewController.delegate = self;
    
    
    UIBarButtonItem* saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Далее"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(actionSaveProfile:)];
    
    UIBarButtonItem* changeButton = [[UIBarButtonItem alloc] initWithTitle:@"Редактировать"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(actionChangeProfileInfo:)];
    
    self.navigationItem.rightBarButtonItem = saveButton;
    self.navigationItem.leftBarButtonItem = changeButton;
    
    [[URMainManager sharedManager] getUserCreatedMeets:self.currentUser onSuccess:^(NSInteger count) {
        self.userCreateMeetsLabel.text = [NSString stringWithFormat:@"%ld", count];
    }];
    
    [[URMainManager sharedManager] getUserFutureMeets:self.currentUser onSuccess:^(NSArray *meetings) {
        [self.meetings addObjectsFromArray:meetings];
        
        NSSortDescriptor* sortByDate = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES];
        [self.meetings sortUsingDescriptors:[NSArray arrayWithObject:sortByDate]];
        
        NSMutableArray* indexPaths = [NSMutableArray array];
        
        for (int i = 0; i < [meetings count]; i ++) {
            NSIndexPath* indexPath = [NSIndexPath indexPathForItem:i inSection:0];
            [indexPaths addObject:indexPath];
        }
        
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView endUpdates];
        
        self.createNewMeetButton.hidden = NO;
        
        self.userFutureMeetsLabel.text = [NSString stringWithFormat:@"%ld", [self.meetings count]];
        
        //[self.tableView reloadData];
        
        [self.activityIndicator stopAnimating];
        [self.activityIndicator removeFromSuperview];

    }];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) setupActivityIndicator {
    
    NSArray *activityTypes = @[@(DGActivityIndicatorAnimationTypeRotatingTrigons)];
    NSArray *sizes = @[@(40.0f)];
    
    for (int i = 0; i < activityTypes.count; i++) {
        self.activityIndicator = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)[activityTypes[i] integerValue] tintColor:[UIColor whiteColor] size:[sizes[i] floatValue]];
        
        CGFloat width = self.view.bounds.size.width / 2.0f;
        CGFloat height = self.view.bounds.size.height / 2.0f;
        self.activityIndicator.frame = CGRectMake(width /2 , height /2  - 70, width, height);
        
        [self.view addSubview:self.activityIndicator];
    }
    
}

- (void) configureScreenNoEdit {
    
    self.userPhoneNumberTextField.delegate  = self;
    self.userPhoneNumberTextField.hidden    = YES;
    self.userChooseScooterButton.hidden     = YES;
    self.createNewMeetButton.hidden         = YES;                                  // while loading - there is activ. indicator in this place
    
    self.userPhotoImageView.image = self.currentUser.photo_200;
    
    self.userNameLabel.text        = [NSString stringWithFormat:@"%@ %@", self.currentUser.firstName, self.currentUser.lastName];
    self.userPhoneNumberLabel.text = self.currentUser.phoneNumber;
    self.userScooterLabel.text     = self.currentUser.scooter;
    
}

#pragma mark - Actions

- (void) actionSaveProfile: (UIBarButtonItem*) item {
    
    if (self.editState == NO) {
        [self.navigationController dismissViewControllerAnimated:[self.navigationController.viewControllers firstObject] completion:nil];
    } else {
        
        NSString* phone = self.userPhoneNumberTextField.text;
        
        if ([phone length] != 14) {
            [[[UIAlertView alloc] initWithTitle:@"Алоха" message:@"Введите корректный номер в виде (050)555-5555"
                                       delegate:self
                              cancelButtonTitle:@"Хорошо"
                              otherButtonTitles:nil, nil]
             show];
            self.userPhoneNumberTextField.hidden = NO;
        }
        
        self.navigationItem.leftBarButtonItem.enabled = YES;
        self.navigationItem.rightBarButtonItem.title = @"Дальше";
    }
    
    self.editState = NO;
    
    if ([self.userPhoneNumberTextField.text length] == 0) {
        self.currentUser.phoneNumber = self.userPhoneNumberLabel.text;
    } else {
        self.currentUser.phoneNumber = self.userPhoneNumberTextField.text;
    }
    
    self.currentUser.scooter = self.userScooterLabel.text;
    
    self.userPhoneNumberTextField.hidden = YES;
    self.userChooseScooterButton.hidden  = YES;
    self.userPhoneNumberLabel.hidden = NO;
    
    self.userPhoneNumberLabel.text       = self.currentUser.phoneNumber;
    self.userScooterLabel.text           = self.currentUser.scooter;
    
    [[URMainManager sharedManager] saveUserOnServerInBackground:self.currentUser];
    
}

- (void) actionChangeProfileInfo: (UIBarButtonItem*) item {
    
    self.editState = YES;

    self.navigationItem.leftBarButtonItem.enabled = NO;
    self.navigationItem.rightBarButtonItem.title = @"Сохранить";
    
    self.userPhoneNumberTextField.hidden = NO;
    self.userPhoneNumberLabel.hidden = YES;
    self.userScooterLabel.hidden = YES;
    self.userChooseScooterButton.hidden = NO;
    
}

- (IBAction)actionCreateNewMeet:(UIButton *)sender {
    
    URNewMeetTableViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"URNewMeetTableViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (IBAction)actionChooseScooter:(UIButton *)sender {
    
    self.userChooseScooterButton.hidden = YES;
    self.userScooterLabel.hidden = NO;
    
    self.popover = [[WYPopoverController alloc] initWithContentViewController:self.scooterModelTabelViewController];
    
    self.popover.delegate = self;
    
    [self.popover setPopoverContentSize:CGSizeMake(250.f, 220.f)];
    
    [self.popover presentPopoverFromRect:sender.frame
                                  inView:self.view
                permittedArrowDirections:WYPopoverArrowDirectionAny
                                animated:YES];
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.meetings count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* meetInfoCell = @"meetInfoCell";
    
    URMainCell *cell = [tableView dequeueReusableCellWithIdentifier:meetInfoCell];
    
    if (!cell) {
        cell = [[URMainCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:meetInfoCell];
    }
    
    [self configureMeetInfoCell:cell forIndexPath:indexPath];
    
    return cell;
}

- (void) configureMeetInfoCell: (URMainCell*) cell forIndexPath: (NSIndexPath*) indexPath {
    
    cell.imageView.image = nil;
    
    PFObject* object = self.meetings[indexPath.row];
    
    URMeet* meet = [[URMeet alloc] initWithObject:object];
    
    cell.photoImageView.image = meet.photo;
    cell.photoImageView.layer.cornerRadius = cell.photoImageView.frame.size.width/2;
    
    cell.firstLineLabel.text     = meet.name;
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm dd/MM yyyy"];
    NSString* dateString = [dateFormatter stringFromDate:meet.date];
    
    cell.secondLineLabel.text    = dateString;
    cell.diclosureTextLabel.text = [NSString stringWithFormat:@""];
    
}

#pragma mark - UITableViewDelegate

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Предстоящие встречи";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64.f;
}

#pragma mark - Popover

- (void) setPopover {
    
    UIColor *greenColor = [UIColor colorWithRed:0.f/255.f green:183.f/255.f blue:157.f/255.f alpha:1];
    
    [WYPopoverController setDefaultTheme:[WYPopoverTheme theme]];
    
    WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
    
    [popoverAppearance setOuterCornerRadius:4];
    [popoverAppearance setOuterShadowBlurRadius:0];
    [popoverAppearance setOuterShadowColor:[UIColor clearColor]];
    [popoverAppearance setOuterShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
    [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setBorderWidth:8];
    [popoverAppearance setArrowHeight:10];
    [popoverAppearance setArrowBase:20];
    
    [popoverAppearance setInnerCornerRadius:4];
    [popoverAppearance setInnerShadowBlurRadius:0];
    [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
    [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setFillTopColor:greenColor];
    [popoverAppearance setFillBottomColor:greenColor];
    [popoverAppearance setOuterStrokeColor:greenColor];
    [popoverAppearance setInnerStrokeColor:greenColor];
    
    UINavigationBar* navBarInPopoverAppearance = [UINavigationBar appearanceWhenContainedIn:[UINavigationController class], [WYPopoverController class], nil];
    [navBarInPopoverAppearance setTitleTextAttributes: @{
                                                         NSForegroundColorAttributeName : [UIColor whiteColor],
                                                         NSShadowAttributeName : [UIColor clearColor],
                                                         NSShadowAttributeName : [NSValue valueWithUIOffset:UIOffsetMake(0, -1)]}];
    
}

#pragma mark - UITextFieldDelegate and validation

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    self.userPhoneNumberTextField.hidden = YES;
    self.userPhoneNumberLabel.text = textField.text;
    [textField resignFirstResponder];
    
    return YES;
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL state;
    state = [self validationPhoneFor:textField inRange:range replacementString:string];
    return state;
    
}

- (BOOL)validationPhoneFor:(UITextField *)textField inRange:(NSRange)range replacementString:(NSString *)string {
    
    NSCharacterSet* validationSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];                      // create new set for only numbers
    
    NSArray* components = [string componentsSeparatedByCharactersInSet:validationSet];                            // arrray with all components of string, which are devided by invalid characters
    
    if ([components count] > 1) {                                                                                 // check if there one or more invalid characters then would be returned NO
        return NO;
    }
    
    NSString* newString = [textField.text stringByReplacingCharactersInRange:range withString:string];            // create new string from textField.text by replacing in range
    
    NSArray* validComponents = [newString componentsSeparatedByCharactersInSet:validationSet];                    // array of valid components
    
    newString = [validComponents componentsJoinedByString:@""];                                                   // string from array with valid components
    
    static const int localNumberMaxLength   = 7;                                                                  // 3 numbers
    static const int areaCodeMaxLength      = 3;
    static const int countryCodeMaxLength   = 0;
    
    if ([newString length] > localNumberMaxLength + areaCodeMaxLength + countryCodeMaxLength) {                   // check if new string lenght > max => NO
        return NO;
    }
    
    NSMutableString* resultString = [NSMutableString string];                                                     // create new resultString
    NSInteger localNumberLength = MIN([newString length], localNumberMaxLength);                                  // every time set localNumberLength MIN of (textField.text and localNumberMaxLength)
    
    if (localNumberLength > 0) {                                                                                  // if it > 0 => create string number substring from index
        
        NSString* number = [newString substringFromIndex:(int)(newString.length) - localNumberLength];
        
        [resultString appendString:number];                                                                       // resultString appends string number
        
        if ([resultString length] > 3) {
            [resultString insertString:@"-" atIndex:3];                                                           // check if result string lenght > 3 insert -
        }
    }
    
    if ([newString length] > localNumberMaxLength) {                                                              // check if new string.length > 3 then
        
        NSInteger areaCodeLength = MIN((int)[newString length] - localNumberMaxLength, areaCodeMaxLength);        // areaCodeLength = - MIN from (newString.length - localNumberMaxLength)-areaCodeMaxLength
        
        NSRange areaRange = NSMakeRange((int)[newString length] - localNumberMaxLength - areaCodeLength, areaCodeLength);   // range from current lenght
        
        NSString* area = [newString substringWithRange:areaRange];
        
        area = [NSString stringWithFormat:@"(%@) ", area];
        
        [resultString insertString:area atIndex:0];
    }
    
    if ([newString length] > localNumberMaxLength + areaCodeMaxLength) {
        
        NSInteger countryCodeLength = MIN((int)[newString length] - localNumberMaxLength - areaCodeMaxLength, countryCodeMaxLength);
        
        NSRange countryCodeRange = NSMakeRange(0, countryCodeLength);
        
        NSString* countryCode = [newString substringWithRange:countryCodeRange];
        
        countryCode = [NSString stringWithFormat:@"+%@ ", countryCode];
        
        [resultString insertString:countryCode atIndex:0];
    }
    
    textField.text = resultString;
    
    return NO;
}

#pragma mark - URScooterModelTableViewControllerDelegate

-(void)setScooterModel:(NSString *)model {
    self.userScooterLabel.text = model;
}



@end
