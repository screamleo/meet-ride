//
//  URScooterRentFormTableViewController.h
//  uRide
//
//  Created by Anton Ivashyna on 7/8/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "URScooter.h"
#import "URRentService.h"

#import "WYPopoverController.h"

#import "URDatePickerPopover.h"

@interface URScooterRentFormTableViewController : UITableViewController 

@property (strong, nonatomic) URScooter* scooter;
@property (strong, nonatomic) URRentService* rentService;

@end
