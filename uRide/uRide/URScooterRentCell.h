//
//  URScooterRentTableViewCell.h
//  uRide
//
//  Created by Anton Ivashyna on 7/8/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface URScooterRentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *timeTextField;
@property (weak, nonatomic) IBOutlet UITextField *dateTextField;

@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *textFieldsCollection;



@end
