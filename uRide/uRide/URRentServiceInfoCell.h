//
//  URCallTableViewCell.h
//  uRide
//
//  Created by Anton Ivashyna on 7/7/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface URRentServiceInfoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfScootersLabel;


@end
