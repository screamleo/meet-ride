//
//  URDistanceCalculator.m
//  uRide
//
//  Created by Anton Ivashyna on 7/17/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//
//**************************************************
//
//  This class helps us calc distances
//
//**************************************************

#import "URDistanceCalculator.h"
#import <CoreLocation/CoreLocation.h>
#import <Parse/Parse.h>

@interface URDistanceCalculator () <CLLocationManagerDelegate>

@end

@implementation URDistanceCalculator

#pragma mark - UITableViewDelegate

- (void) calculateDistanceToDestinationPoint: (PFGeoPoint*) destinationLocation
                                   onSuccess: (void(^)(CLLocationDistance distance)) success
                                   onFailure: (void(^)(NSError* error)) failure {
    
    self.locationManager = [[CLLocationManager alloc ] init];
    self.locationManager.delegate = self;
    
    [self.locationManager requestAlwaysAuthorization];
    [self.locationManager startUpdatingLocation];
    
    CLLocation* userLocation = self.location;
    
    CLLocationCoordinate2D destinationCoordinate = CLLocationCoordinate2DMake(destinationLocation.latitude,
                                                                              destinationLocation.longitude);
    
    CLLocation* destLocation = [[CLLocation alloc] initWithLatitude:destinationCoordinate.latitude longitude:destinationCoordinate.longitude];
    
    CLLocationDistance distance = [userLocation distanceFromLocation:destLocation];
    
    if (success){
        success(distance/1000);             // we need to devide on 1000 cause we get distance in metres
    }
    
}

#pragma mark - Loaction methods

-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    [self.locationManager requestAlwaysAuthorization];
    [self.locationManager startUpdatingLocation];
    
    self.location = locations.lastObject;
}

-(void)statLocationManager {
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [self.locationManager startUpdatingLocation];
    [self.locationManager requestWhenInUseAuthorization];
    
}

@end
