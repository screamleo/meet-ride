//
//  URDatePickerViewController.h
//  uRide
//
//  Created by Anton Ivashyna on 7/8/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol URDatePickerPopover;

@interface URDatePickerPopover : UIViewController

@property (weak, nonatomic) id <URDatePickerPopover> delegate;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

- (IBAction)rentDateChangedAction:(UIDatePicker *)sender;

@end


@protocol URDatePickerPopover <NSObject>

@required
- (void) setDateFromPopover: (NSDate*) date;

@end
