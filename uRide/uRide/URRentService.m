//
//  URRentService.m
//  uRide
//
//  Created by Anton Ivashyna on 7/8/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import "URRentService.h"
#import "URCity.h"
#import "URScooter.h"


@implementation URRentService

@dynamic addressLalitude;
@dynamic addressLongitude;
@dynamic name;
@dynamic number;
@dynamic rating;
@dynamic logo;
@dynamic rentPhoto1;
@dynamic rentPhoto2;
@dynamic rentPhoto3;
@dynamic rentPhoto4;
@dynamic rentPriceForGiorno;
@dynamic rentPriceForVino;
@dynamic rentPriceForJoker;
@dynamic city;
@dynamic scooters;

@synthesize distanceFromUser;

@end
