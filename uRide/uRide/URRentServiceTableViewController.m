//
//  URRentServiceTableViewController.m
//  uRide
//
//  Created by Anton Ivashyna on 7/6/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//
//*********************************************************
//
//  Here we show detail info for rent service.
//
//*********************************************************

#import "URRentServiceTableViewController.h"
#import "URScooter.h"

#import "URMainCell.h"
#import "URRentServiceInfoCell.h"

#import "URScooterRentFormTableViewController.h"

@implementation URRentServiceTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = self.rentService.name;
    
    self.scooterPhotosData = @[self.rentService.rentPhoto1, self.rentService.rentPhoto2,
                               self.rentService.rentPhoto3, self.rentService.rentPhoto4];
            
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.scooterPhotosData count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* scooterPhotoCellID = @"scooterPhotoCellID";
    
    UICollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:scooterPhotoCellID forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[UICollectionViewCell alloc] initWithFrame:CGRectMake(10, 10, 150, 150)];
    }
    
    [cell.layer setBorderWidth:2.0f];
    [cell.layer setBorderColor:[UIColor colorWithRed:0/255.f green:180/255.f blue:156/255.f alpha:1].CGColor];
    
    UIImageView* scooterPhotoImageView = (UIImageView*)[cell viewWithTag:100];
    
    NSData* scooterPhotoData = self.scooterPhotosData[indexPath.row];
    
    UIImage* scooterPhoto = [UIImage imageWithData:scooterPhotoData];
    
    scooterPhotoImageView.image = scooterPhoto;
    
    return cell;
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.rentService.scooters allObjects] count] + 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(UITableViewCell*) tableView: (UITableView*) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    if (indexPath.row == 0) {
        
        static NSString* callRentServiceCellID = @"callRentServiceCellID";
        
        URRentServiceInfoCell* callCell = [tableView dequeueReusableCellWithIdentifier:callRentServiceCellID forIndexPath:indexPath];
        
        if (!callCell) {
            callCell = [[URRentServiceInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:callRentServiceCellID];
        }
        
        callCell.distanceLabel.text = [self.distanceFromUser substringToIndex:[self.distanceFromUser length] - 3];
                
        return callCell;
        
    } else {
        
        static NSString* scooterCellID = @"scooterCellID";
        
        URMainCell* scooterCell = [tableView dequeueReusableCellWithIdentifier:scooterCellID forIndexPath:indexPath];
        
        if (!scooterCell) {
            scooterCell = [[URMainCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:scooterCellID];
        }
                
        [self configureCell:scooterCell atIndexPath:indexPath];
        
        return scooterCell;
        
    }
    
}

-(void)configureCell:(URMainCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSArray* rentServiceScooters = [self.rentService.scooters allObjects];
    
    URScooter* scooter = rentServiceScooters[indexPath.row - 1];
    
    UIImageView* imageView = (UIImageView*)[cell viewWithTag:400];
    
    cell.firstLineLabel.text  = scooter.name;
    cell.secondLineLabel.text = scooter.name;
    imageView.image = [UIImage imageWithData:scooter.photo1];
    [imageView.layer setCornerRadius:imageView.frame.size.width/2];
    imageView.clipsToBounds = YES;
    
    // setting of prices to rent
    
    [self setScooterPriceInCell:cell forScooter:scooter];
}

- (void) setScooterPriceInCell: (URMainCell*) cell forScooter: (URScooter*) scooter {
    
    if ([scooter.name isEqualToString:@"Honda Giorno"]) {
        cell.diclosureTextLabel.text = [NSString stringWithFormat:@"%d грн",
                                        self.rentService.rentPriceForGiorno.intValue];
        
    } else if ([scooter.name isEqualToString:@"Yamaha Vino"]) {
        cell.diclosureTextLabel.text = [NSString stringWithFormat:@"%d грн",
                                        self.rentService.rentPriceForVino.intValue];
        
    } else if ([scooter.name isEqualToString:@"Honda Joker"]) {
        cell.diclosureTextLabel.text = [NSString stringWithFormat:@"%d грн",
                                        self.rentService.rentPriceForJoker.intValue];
    }
    
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray* rentServiceScooters = [self.rentService.scooters allObjects];
    URScooter* scooter = rentServiceScooters[indexPath.row - 1];
    
    URScooterRentFormTableViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"scooterFormID"];
    vc.scooter = scooter;
    vc.rentService = self.rentService;
    NSLog(@"%@", self.rentService.name);
    [self.navigationController pushViewController:vc animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.row == 0 ? NO : YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.row == 0 ? 84.f : 64.f;
}


@end
