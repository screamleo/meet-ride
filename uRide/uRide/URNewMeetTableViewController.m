//
//  URNewMeetTableViewController.m
//  uRide
//
//  Created by Anton Ivashyna on 7/17/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import "URNewMeetTableViewController.h"

#import "WYPopoverController.h"
#import "URDatePickerPopover.h"
#import "URTimePickerPopover.h"
#import "URMapViewController.h"

#import "URMeet.h"
#import "URUser.h"

#import "URMainManager.h"

#import <Parse/Parse.h>

typedef enum {
    URTextFieldName,
    URTextFieldTime,
    URTextFieldDate
} URTextField;

@interface URNewMeetTableViewController () <UITextFieldDelegate, WYPopoverControllerDelegate, URTimePickerPopoverDelegate,
                                            URDatePickerPopover, UIImagePickerControllerDelegate,
                                            UINavigationControllerDelegate, URMapViewControllerDelegate>

@property (strong, nonatomic) WYPopoverController* pickerPopoverController;
@property (strong, nonatomic) URDatePickerPopover* controllerForDatePickerView;
@property (strong, nonatomic) URTimePickerPopover* controllerForTimePickerView;
@property (strong, nonatomic) URMapViewController* mapViewController;

@property (strong, nonatomic) NSDate* meetDate;

@property (strong, nonatomic) PFGeoPoint* meetPoint;

@property (strong, nonatomic) UIImagePickerController* picker;

@end

@implementation URNewMeetTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem* backItem = [[UIBarButtonItem alloc] initWithTitle:@"Назад"
                                                                 style:UIBarButtonItemStylePlain
                                                                target:self
                                                                action:@selector(actionPopViewController:)];
    
    self.navigationItem.leftBarButtonItem = backItem;
    
    //self.meetPoint = [[PFGeoPoint alloc] init];
    
    self.meetDate = [NSDate dateWithTimeIntervalSinceNow:0];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.meetNameTextField.delegate = self;
    self.meetTimeTextField.delegate = self;
    self.meetDateTextField.delegate = self;
    
    self.controllerForDatePickerView = [self.storyboard instantiateViewControllerWithIdentifier:@"datePickerStoryboardID"];
    self.controllerForDatePickerView.delegate = self;
    
    self.controllerForTimePickerView = [self.storyboard instantiateViewControllerWithIdentifier:@"timePickerStoryboardID"];
    self.controllerForTimePickerView.delegate = self;
    
}

#pragma mark - URDatePickerViewDelegate

- (void) setDateFromPopover:(NSDate *)date {
    
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc]init];
    [dateFormater setDateFormat:@"dd-M-yyyy"];
    self.meetDateTextField.text = [dateFormater stringFromDate:date];
    
    NSDate *referenceDate = self.meetDate;
    NSTimeInterval timeInterval = [date timeIntervalSinceDate:referenceDate];
    self.meetDate = [NSDate dateWithTimeInterval:timeInterval sinceDate:self.meetDate];
    
}

#pragma mark - URTimePickerViewDelegate

- (void) setTimeFromPopover:(NSDate *)date {
    
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc]init];
    [dateFormater setDateFormat:@"HH:mm"];
    self.meetTimeTextField.text = [dateFormater stringFromDate:date];
    
    NSDate *referenceDate = self.meetDate;
    NSTimeInterval timeInterval = [date timeIntervalSinceDate:referenceDate];
    self.meetDate = [NSDate dateWithTimeInterval:timeInterval sinceDate:self.meetDate];
    
}

#pragma mark - URMapViewControllerDelegate

-(void)setPointForNewMeet:(PFGeoPoint*)point {
    self.meetPoint = point;
}

#pragma mark - Actions

- (void) actionPopViewController: (UIBarButtonItem*) item {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionOpenMap:(id)sender {
    
    self.mapViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"URMapViewController"];
    self.mapViewController.delegate = self;

    self.mapViewController.meetName = self.meetNameTextField.text;
    self.mapViewController.dateString = [NSString stringWithFormat:@"%@ %@", self.meetDateTextField.text, self.meetTimeTextField.text];
    self.mapViewController.comeFromNewMeet = YES;
    [self.navigationController pushViewController:self.mapViewController animated:YES];
    
}

- (IBAction)actionOpenTimePopover:(id)sender {
    
    UIButton* button = (UIButton*)sender;
    
    self.pickerPopoverController = [[WYPopoverController alloc] initWithContentViewController:self.controllerForTimePickerView];
    self.pickerPopoverController.delegate = self;
    [self.pickerPopoverController setPopoverContentSize:CGSizeMake(250.f, 200.f)];
    
    [self.pickerPopoverController presentPopoverFromRect:button.bounds
                                                  inView:button
                                permittedArrowDirections:WYPopoverArrowDirectionDown
                                                animated:YES];
    
}

- (IBAction)actionOpenDatePopover:(id)sender {
    
    UIButton* button = (UIButton*)sender;
    
    self.pickerPopoverController = [[WYPopoverController alloc] initWithContentViewController:self.controllerForDatePickerView];
    self.pickerPopoverController.delegate = self;
    [self.pickerPopoverController setPopoverContentSize:CGSizeMake(250.f, 200.f)];
    
    [self.pickerPopoverController presentPopoverFromRect:button.bounds
                                                  inView:button
                                permittedArrowDirections:WYPopoverArrowDirectionDown
                                                animated:YES];
    
}

- (IBAction)actionSaveNewMeet:(id)sender {
    
    NSLog(@"actionSaveNewMeet");
    
    URUser* user = [URUser currentUser];
    
    URMeet* meet = [[URMeet alloc] init];
    
    meet.city    = user.city;
    meet.creator = [NSString stringWithFormat:@"%@", user.userID];
    meet.date    = self.meetDate;
    meet.name    = self.meetNameTextField.text;
    meet.photo   = self.photoImageView.image;
    meet.place   = self.meetPoint;

    if (meet.city && meet.creator && meet.date &&
        meet.name && [meet.name length] > 3 && meet.photo && meet.place) {
        
        [[URMainManager sharedManager] saveNewMeet:meet onSuccess:^(bool saved){
            
            if (saved == YES) {
                [[[UIAlertView alloc] initWithTitle:@"Конгратюлейшнз" message:@"Встреча успешно сохранена"
                                           delegate:self
                                  cancelButtonTitle:@"Хорошо"
                                  otherButtonTitles:nil, nil]
                 show];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }
            
        } onFailure:^(NSError *error) {
            
            [[[UIAlertView alloc] initWithTitle:@"Упс" message:@"Проверьте соединение с интернетом"
                                       delegate:self
                              cancelButtonTitle:@"Ок"
                              otherButtonTitles:nil, nil]
             show];
            
        }];
        
    } else {
        
        [[[UIAlertView alloc] initWithTitle:@"Упс" message:@"Скорее всего, Вы что-то не ввели. Проверяйте!"
                                   delegate:self
                          cancelButtonTitle:@"Больше так не буду."
                          otherButtonTitles:nil, nil]
         show];
        
    }
}

- (IBAction)actionAddPhoto:(id)sender {
    
    self.picker = [[UIImagePickerController alloc] init];
    self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    self.picker.delegate = self;
    
    [self.navigationController presentViewController:self.picker animated:YES completion:nil];
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return textField.tag == URTextFieldName ? YES : NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.meetNameTextField resignFirstResponder];
    return YES;
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    self.photoImageView.image = chosenImage;
    self.addPhotoButton.hidden = YES;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


@end
