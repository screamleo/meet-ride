//
//  AppDelegate.m
//  uRide
//
//  Created by Leo Lashkevich on 6/23/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import "URAppDelegate.h"
#import "URCoreDataManager.h"

#import "URLogInVC.h"
#import "TipPageViewController.h"
#import <Parse/Parse.h>

@interface URAppDelegate ()

@end

@implementation URAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // register app o parse.com
    [Parse setApplicationId:@"zS153w8U3vvgJQpGfCJPRQsaSqIavlYYAGuWuMU9"
                  clientKey:@"HTsmEsViGbpDPwpUdUiib1vs0xZ14wOBzF8speFb"];
    
    self.window.tintColor = [UIColor colorWithRed:88/255.f
                                            green:197/255.f
                                             blue:168/255.f
                                            alpha:1];
    UIPageControl *pageControl = [UIPageControl appearanceWhenContainedIn:[TipPageViewController class], nil];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    
    [self configureMainScreen];
    
    return YES;
}

- (void) configureMainScreen {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    URLogInVC *viewController = [storyboard instantiateViewControllerWithIdentifier:@"URLoginVC"];
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:viewController];
    self.window.rootViewController = nav;

}

- (BOOL) VKTokenAvailable {
    
    NSDate* tokenExpirationDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"tokenExpirationDate"];
    NSDate* nowDate = [NSDate date];
    
    return [tokenExpirationDate compare:nowDate] == NSOrderedDescending;
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    
    [[URCoreDataManager sharedManager] saveContext];
}


@end
