//
//  URMainManager.m
//  uRide
//
//  Created by Leo Lashkevich on 6/24/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//
//*********************************************************
//
//  Manager to work with server methods (vk.com and parse.com).
//
//*********************************************************
#import "URMainManager.h"
#import "AFNetworking.h"

#import "URLoginViewController.h"

#import "URAccessToken.h"

#import "URUser.h"
#import "URCity.h"
#import "URMeet.h"

#import <Parse/Parse.h>

@interface URMainManager ()

@property (strong, nonatomic) AFHTTPRequestOperationManager* requestOperationManager;

@end

@implementation URMainManager

+(URMainManager *)sharedManager{
    static URMainManager* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[super alloc]initUniqueInstance];;
    });
    return sharedInstance;
}

-(instancetype)initUniqueInstance{
    self = [super init];
    if (self) {
        NSURL* url = [NSURL URLWithString:@"https://api.vk.com/method/"];
        
        self.requestOperationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    }
    return self;
}

#pragma mark - VK API Methods

- (void) autorizeUser:(void(^)(URUser* user)) completion {                          // allows user to login with vk.com
    
    URLoginViewController* vc = [[URLoginViewController alloc]
                                 initWithCompletionBlock:^(URAccessToken *token) {
                                     self.accessToken = token;
        
                                     if (token) {
            
                                         [self getUser:self.accessToken.userID onSuccess:^(URUser *user) {
                                             
                                             completion(user);
                                             [self saveDefaultsForUser:user];
                                             
                                            } onFailure:^(NSError *error, NSInteger statusCode) {
                                                
                                            }];
                                    }
                                }];
    
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:vc];
    UIViewController* mainVC    = [[[[UIApplication sharedApplication] windows] firstObject]rootViewController];
    
    [mainVC presentViewController:nav animated:YES completion:nil];
    
}

- (void) logOutUserWithExitBlock: (void(^)(NSString* success)) success {            // while logout - we have to clear cookie!
    
    NSString *logout = @"http://api.vk.com/oauth/logout";
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:logout]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                       timeoutInterval:60.0];
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil
                                                             error:nil];
    
    if (success) {
        success(@"User log out success");
        self.accessToken = nil;
        [self deleteCookies];
        if(responseData){
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"firstName"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"lastName"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isLoggedIn"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"tokenExpirationDate"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"firstTimeLaunch"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
}

- (void) getUser:(NSString*) userID
       onSuccess:(void(^)(URUser* user)) success
       onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {              // here we could get info about user (name, lastname, photo)
    
    NSDictionary* params = @{
                             @"user_ids" : userID,
                             @"order" : @"name",
                             @"fields" : @"photo_200",
                             @"name_case" : @"nom"};
    
    [self.requestOperationManager
     GET:@"users.get"
     parameters:params
     success:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSLog(@"JSON: %@", responseObject);
         
         NSArray* dictsArray = [responseObject objectForKey:@"response"];
         
         NSMutableArray* objectsArray = [NSMutableArray array];
         
         for (NSDictionary* dict in dictsArray) {
             URUser* user = [[URUser alloc] initWithServerResponse:dict];
             [objectsArray addObject:user];
         }
         
         if (success) {
             success([objectsArray objectAtIndex:0]);
         }
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         if (failure) {
             failure(error, operation.response.statusCode);
         }
         
     }];
    
}

- (void) deleteCookies {                                                                        // MUST HAVE!
    
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        
        NSString* domainName = [cookie domain];
        NSRange domainRange = [domainName rangeOfString:@"vk.com"];
        
        if(domainRange.length > 0) {
            [storage deleteCookie:cookie];
        }
    }
    
}

#pragma mark - Parse.com methods
                                                                             // this method help us to get meets in what user is a participant
- (void) getUserFutureMeets: (URUser*) user onSuccess:(void(^)(NSMutableArray* meetings)) success {
    
    PFQuery* query = [PFQuery queryWithClassName:@"MeetClass"];             //get all objects of class
    [query includeKey:@"participants"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *PF_NULLABLE_S objects, NSError *PF_NULLABLE_S error) {
        
        NSMutableArray* userMeetings = [NSMutableArray array];              // create an empty array
        
        for (PFObject* meet in objects) {                                   // 'objects' - is dictionary
            
            NSArray* participants = [meet objectForKey:@"participants"];    // in this case we get all participants of 'meet'
            
            for (NSString* participantUserID in participants) {             // 'participants' - array of strings (userID`s)
                
                NSString* partString = [NSString stringWithFormat:@"%@", participantUserID];
                                
                NSString* userIDString = [NSString stringWithFormat:@"%@", user.userID];
                
                if ([partString isEqualToString:userIDString]) {
                    [userMeetings addObject:meet];
                }
            }
        }
        
        if (success) {
            success(userMeetings);
        }
    }];
    
}

- (void) getUserCreatedMeets: (URUser*) user onSuccess:(void(^)(NSInteger count)) success {
    
    PFQuery* query = [PFQuery queryWithClassName:@"MeetClass"];             //get all objects of class
    [query whereKey:@"creator" equalTo:[NSString stringWithFormat:@"%@", user.userID]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *PF_NULLABLE_S objects, NSError *PF_NULLABLE_S error) {
        
        if (success) {
            success([objects count]);
        }
        
    }];
}

- (void) getMeetsForCity: (URCity*) city onSuccess:(void(^)(NSArray* meetings)) success {
    
    URUser* user = [URUser currentUser];
    
    PFQuery* query = [PFQuery queryWithClassName:@"MeetClass"];             //get all objects of class
    [query whereKey:@"city" equalTo:user.city];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *PF_NULLABLE_S objects, NSError *PF_NULLABLE_S error) {
        
        if (success) {
            success(objects);
        }
    }];
}

- (void) getParticipantsForMeet: (URMeet*) meet onSuccess:(void(^)(NSArray *participants, NSArray* participanstIDs)) success {
    
    PFQuery* query = [PFQuery queryWithClassName:@"MeetClass"];
    
    NSString* meetIDString = [NSString stringWithFormat:@"%@", meet.meetID];
    
    [query whereKey:@"objectId" equalTo:meetIDString];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray* objects, NSError* error) {
        
        NSArray* participantIDs = [[objects valueForKey:@"participants"] firstObject];
        
        NSMutableArray* participantsIDsArray = [NSMutableArray array];
        
        for (id obj in participantIDs) {                                                // we have to do type formatting to NSString, cause not all object userID - NSString and app can crash
            NSString* partID = [NSString stringWithFormat:@"%@", obj];
            [participantsIDsArray addObject:partID];
        }
        
        [self getUsersFromParseWithIDs:participantsIDsArray onSuccess:^(NSArray *array) {
            
            if (success) {
                success(array, participantIDs);
            }
        }];
    }];
}

- (void) getUsersFromParseWithIDs: (NSArray*) userIDs onSuccess:(void(^)(NSArray* array)) success {
    
    NSMutableArray* arr = [NSMutableArray array];
    
    for (id participantID in userIDs) {
        
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        formatter.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *userIDNumber = [formatter numberFromString:participantID];
        
        [arr addObject:userIDNumber];
    }
    
    PFQuery* query = [PFQuery queryWithClassName:@"UserClass"];
    
    [query whereKey:@"userID" containedIn:arr];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *PF_NULLABLE_S objects, NSError *PF_NULLABLE_S error) {
        
        if (success) {
            success(objects);
        }
        
    }];
    
}

- (void) addNewParticipantWithID:(NSString*) participantID
                          toMeet: (URMeet*) meet
                       onSuccess: (void (^)(bool saved)) success
                       onFailure: (void(^)(NSError* error)) failure {
    
    PFQuery* query = [PFQuery queryWithClassName:@"MeetClass"];
    
    [query whereKey:@"objectId" equalTo:meet.meetID];
    
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *PF_NULLABLE_S object,  NSError *PF_NULLABLE_S error) {
        [object addUniqueObject:participantID forKey:@"participants"];
        [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *PF_NULLABLE_S error) {
            
            if (succeeded) {
                NSLog(@"succeeded");
            }
            
        }];
    }];
}

#pragma mark - NSUserDefaults

- (void) saveDefaultsForUser: (URUser*) user {
    
    BOOL isLoggedIn = YES;
    NSDate* tokenExpirationDate = self.accessToken.expirationDate;

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setBool:isLoggedIn forKey:@"isLoggedIn"];
    [defaults setObject:tokenExpirationDate forKey:@"tokenExpirationDate"];

    [defaults setObject:user.firstName      forKey:@"firstName"];
    [defaults setObject:user.lastName       forKey:@"lastName"];
    [defaults setObject:@"еще не указан"    forKey:@"phoneNumber"];
    [defaults setObject:@"еще не указан"    forKey:@"scooter"];
    [defaults setObject:user.userID         forKey:@"userID"];
    
    [defaults setObject:UIImagePNGRepresentation(user.photo_200) forKey:@"photo_200"];
    
    [defaults synchronize];
    
    [self saveUserOnServerInBackground:user];
    
}

- (void) saveUserOnServerInBackground: (URUser*) user {
    
    PFQuery* query = [PFQuery queryWithClassName:@"UserClass" predicate:[NSPredicate predicateWithFormat:@"userID = %@", user.userID]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *PF_NULLABLE_S objects, NSError *PF_NULLABLE_S error) {

        if ([objects count] == 0) {
            PFObject* newUser = [PFObject objectWithClassName:@"UserClass"];
            
            [newUser setValue:user.firstName forKey:@"firstName"];
            [newUser setValue:user.lastName  forKey:@"lastName"];
            [newUser setValue:user.scooter   forKey:@"phoneNumber"];
            [newUser setValue:user.scooter   forKey:@"scooter"];
            [newUser setValue:user.userID    forKey:@"userID"];
            
            PFFile* userPhoto = [PFFile fileWithData:UIImagePNGRepresentation(user.photo_200)];
            
            [newUser setValue:userPhoto forKey:@"photo"];
            
            [newUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *PF_NULLABLE_S error) {
                if (succeeded) {
                }
            }];
        }
    }];
}

- (void) saveNewMeet: (URMeet*) meet
           onSuccess: (void (^)(bool saved)) success
           onFailure: (void(^)(NSError* error)) failure {
    
    URUser* user = [URUser currentUser];
    
    PFObject* newMeetObject = [PFObject objectWithClassName:@"MeetClass"];
    
    [newMeetObject setValue:meet.city forKey:@"city"];
    [newMeetObject setValue:meet.name forKey:@"name"];
    [newMeetObject setValue:meet.date forKey:@"date"];
    
    PFFile* photo = [PFFile fileWithData:UIImagePNGRepresentation(meet.photo)];
    
    [newMeetObject setValue:photo forKey:@"photo"];
    [newMeetObject setValue:meet.place forKey:@"place"];
    [newMeetObject setValue:meet.creator forKey:@"creator"];
    [newMeetObject addUniqueObject:user.userID forKey:@"participants"];
    
    [newMeetObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *PF_NULLABLE_S error) {
        
    }];
    
    if (success) {
        success(YES);
    }
    
}


@end
