//
//  CoreDataManager.h
//  CyberSecurityDigest
//
//  Created by Leo Lashkevich on 5/24/15.
//  Copyright (c) 2015 Leo Lashkevich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface URCoreDataManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+(URCoreDataManager*) sharedManager;

+(instancetype)alloc __attribute__((unavailable("alloc is unavailable, please use sharedManager")));
+(instancetype)new __attribute__((unavailable("New is unavailable, please use sharedManager")));
-(instancetype)init __attribute__((unavailable("init is unavailable, please use sharedManager")));

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end
