//
//  URMainManager.h
//  uRide
//
//  Created by Leo Lashkevich on 6/24/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <Foundation/Foundation.h>

@class URAccessToken, URUser, URCity, URMeet;


@interface URMainManager : NSObject

@property (strong, nonatomic) URAccessToken* accessToken;

+(URMainManager*)sharedManager;

+(instancetype)alloc __attribute__((unavailable("alloc is unavailable, please use sharedManager")));
+(instancetype)new __attribute__((unavailable("New is unavailable, please use sharedManager")));
-(instancetype)init __attribute__((unavailable("init is unavailable, please use sharedManager")));

- (void) autorizeUser:(void(^)(URUser* user)) completion;

- (void) logOutUserWithExitBlock: (void(^)(NSString* success)) success;

- (void) saveUserOnServerInBackground: (URUser*) user;

- (void) getUserFutureMeets: (URUser*) user onSuccess:(void(^)(NSMutableArray* meetings)) success;

- (void) getMeetsForCity: (URCity*) city onSuccess:(void(^)(NSArray* meetings)) success;

- (void) getUserCreatedMeets: (URUser*) user onSuccess:(void(^)(NSInteger count)) success;

- (void) saveNewMeet: (URMeet*) meet
           onSuccess: (void(^)(bool saved)) success
           onFailure: (void(^)(NSError* error)) failure;

- (void) getParticipantsForMeet: (URMeet*) meet
                      onSuccess:(void(^)(NSArray* participants, NSArray* participanstIDs)) success;

- (void) addNewParticipantWithID:(NSString*) participantID
                          toMeet: (URMeet*) meet
                       onSuccess: (void (^)(bool saved)) success
                       onFailure: (void(^)(NSError* error)) failure;

@end
