//
//  URMapViewController.h
//  uRide
//
//  Created by Anton Ivashyna on 7/17/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <Parse/Parse.h>

@class MKMapView;
@class CLLocationManager, CLLocation;

@protocol URMapViewControllerDelegate;

@interface URMapViewController : UIViewController

@property (weak, nonatomic) id <URMapViewControllerDelegate> delegate;

@property (strong, nonatomic) NSString* meetName;               // to show meetName, when create new pin on map
@property (strong, nonatomic) NSString* dateString;

@property (assign, nonatomic) BOOL comeFromNewMeet;             // to know where from user came. If from 'new meet' - he ca create pin, else - can`t do this.
                                                                // He can only see pins from 'objectsToDisplay'

@property (strong, nonatomic) NSArray* objectsToDisplay;        // it can be meet, in what user want to come, or rent service

@property (weak, nonatomic) IBOutlet MKMapView* mapView;

@property (strong, nonatomic) CLLocationManager* locationManager;
@property (strong, nonatomic) CLLocation* location;

@end

@protocol URMapViewControllerDelegate <NSObject>

-(void)setPointForNewMeet:(PFGeoPoint*)point;

@end
