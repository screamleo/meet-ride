//
//  URMeet.h
//  uRide
//
//  Created by Anton Ivashyna on 7/15/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@class URUser, Parse;

@interface URMeet : NSObject

@property (strong, nonatomic) NSString* city;
@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* time;
@property (strong, nonatomic) NSString* creator;
@property (strong, nonatomic) NSString* meetID;

@property (strong, nonatomic) UIImage* photo;

@property (strong, nonatomic) NSMutableArray* participants;

@property (strong, nonatomic) NSDate* date;
@property (strong, nonatomic) PFGeoPoint* place;
@property (assign, nonatomic) double distance;

- (instancetype) initWithObject: (PFObject*) object;

@end
