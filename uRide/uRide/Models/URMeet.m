//
//  URMeet.m
//  uRide
//
//  Created by Anton Ivashyna on 7/15/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import "URMeet.h"
#import "URMainManager.h"

@implementation URMeet

@synthesize name;
@synthesize date;
@synthesize time;
@synthesize photo;
@synthesize place;
@synthesize creator;
@synthesize participants;

- (instancetype) initWithObject: (PFObject*) object {
    
    self = [super init];
    if (self) {
        
        self.name         = [object valueForKey:@"name"];
        self.date         = [object valueForKey:@"date"];
        self.time         = [object valueForKey:@"time"];
        self.meetID       = [object valueForKey:@"objectId"];
        
        PFFile* file = [object objectForKey:@"photo"];
        NSData* data = [file getData];
        self.photo = [UIImage imageWithData:data];
        
        self.place        = [object valueForKey:@"place"];
        self.creator      = [object valueForKey:@"creator"];
        self.participants = [object valueForKey:@"participants"];
        self.place        = [object valueForKey:@"place"];

    }
    
    return self;
    
}

@end
