//
//  URUser.m
//  uRide
//
//  Created by Anton Ivashyna on 7/14/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import "URUser.h"

#import "URMainManager.h"

#import <Parse/Parse.h>

@interface URUser ()

@property (strong, nonatomic) URUser* user;

@end

@implementation URUser

@synthesize userID;

- (id) initWithServerResponse:(NSDictionary*) responseObject {
    
    self = [super init];
    if (self) {
        
        self.firstName  = [responseObject objectForKey:@"first_name"];
        self.lastName   = [responseObject objectForKey:@"last_name"];
        self.userID     = [responseObject objectForKey:@"uid"];
        
        NSString* photo_200UrlString = [responseObject objectForKey:@"photo_200"];
        
        if (photo_200UrlString) {
            
            NSURL* imageURL = [NSURL URLWithString:photo_200UrlString];
            NSData* data    = [NSData dataWithContentsOfURL:imageURL];
            
            UIImage *logoImage = [UIImage imageWithData:data];
            self.photo_200 = logoImage;
        }
    }
    return self;
}

+ (URUser*) currentUser {
    
    URUser* user = [[URUser alloc] init];
    
    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"photo_200"];
    UIImage* image = [UIImage imageWithData:imageData];
    user.photo_200 = image;
    
    user.city           = [[NSUserDefaults standardUserDefaults] objectForKey:@"city"];
    user.firstName      = [[NSUserDefaults standardUserDefaults] objectForKey:@"firstName"];
    user.lastName       = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastName"];
    user.phoneNumber    = [[NSUserDefaults standardUserDefaults] objectForKey:@"phoneNumber"];
    
    NSLog(@"in deafults - %@", user.phoneNumber);
    user.scooter        = [[NSUserDefaults standardUserDefaults] objectForKey:@"scooter"];
    user.userID         = [[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    
    return user;
}

#pragma mark - Setters

//it is better to change values on server with calling only 1 method (I mean setter) but not 2. so we should call server method [setValue: forKey:] in this setter

- (void)setFirstName:(NSString *)firstName {
    _firstName = firstName;
    [[NSUserDefaults standardUserDefaults] setValue:_firstName forKey:@"firstName"];
    
    [self getQueryAndSetObject:_firstName forKey:@"firstName"];
}

- (void) setLastName:(NSString *)lastName {
    _lastName = lastName;
    
    [[NSUserDefaults standardUserDefaults] setValue:_lastName forKey:@"lastName"];
    [self getQueryAndSetObject:_lastName forKey:@"lastName"];
}

-(void)setPhoneNumber:(NSString *)phoneNumber {
    _phoneNumber = phoneNumber;
    
    [[NSUserDefaults standardUserDefaults] setValue:_phoneNumber forKey:@"phoneNumber"];
    [self getQueryAndSetObject:_phoneNumber forKey:@"phoneNumber"];
}

-(void)setPhoto_200:(UIImage *)photo_200 {
    _photo_200 = photo_200;
    
    [[NSUserDefaults standardUserDefaults] setValue:UIImagePNGRepresentation(_photo_200) forKey:@"photo_200"];
    [self getQueryAndSetObject:_photo_200 forKey:@"photo"];
}

-(void)setScooter:(NSString *)scooter {
    _scooter = scooter;
    
    [[NSUserDefaults standardUserDefaults] setValue:_scooter forKey:@"scooter"];
    [self getQueryAndSetObject:_scooter forKey:@"scooter"];
}

-(void)setCity:(NSString *)city {
    _city = city;
    
    [[NSUserDefaults standardUserDefaults] setValue:_city forKey:@"city"];
    [self getQueryAndSetObject:_city forKey:@"city"];
}

- (void) getQueryAndSetObject: (id) objectToSet
                       forKey: (NSString*) objectKey {
    
     if (userID) {
     
         PFQuery* query = [PFQuery queryWithClassName:@"UserClass"];
         [query whereKey:@"userID" equalTo:self.userID];
         [query findObjectsInBackgroundWithBlock:^(NSArray *PF_NULLABLE_S objects, NSError *PF_NULLABLE_S error) {
             PFObject* object = [objects firstObject];
             [object setObject:objectToSet forKey:objectKey];
             [object saveInBackground];
         }];
     }
}



@end
