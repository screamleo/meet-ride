//
//  URTabBarVC.h
//  uRide
//
//  Created by Leo Lashkevich on 7/5/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "URCity.h"

@interface URTabBarVC : UITabBarController
@property (strong, nonatomic) URCity* city;
@end
