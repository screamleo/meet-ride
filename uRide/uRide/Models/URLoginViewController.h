//
//  URLoginViewController.h
//  uRide
//
//  Created by Anton Ivashyna on 7/14/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>

@class URAccessToken;

typedef void(^URLoginCompletionBlock)(URAccessToken* token);

@interface URLoginViewController : UIViewController

- (id) initWithCompletionBlock:(URLoginCompletionBlock) completionBlock;

@end
