//
//  URTabView.m
//  uRide
//
//  Created by Leo Lashkevich on 7/5/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import "URTabView.h"

@implementation URTabView

+(URTabView*)createWithFrame:(CGRect)frame{
    
   URTabView *tBar = [[[NSBundle mainBundle] loadNibNamed:@"URTabView" owner:nil options:nil] objectAtIndex:0];
    tBar.frame = frame;
    tBar.meetButton.frame = CGRectMake(0, 0,
                                       frame.size.width/2 - 5,
                                       frame.size.height);
    tBar.rentButton.frame = CGRectMake(frame.size.width/2,
                                       0, frame.size.width/2,
                                       frame.size.height);
    
    [tBar.delegate selectTabAtIndex:0];
    [tBar.meetButton setSelected:YES];
    [tBar.rentButton setSelected:NO];
    return tBar;
}

-(IBAction)selectTab:(id)sender {
    if ([sender isEqual:self.meetButton]) {
        [self.delegate selectTabAtIndex:0];
        [self.meetButton setSelected:YES];
        [self.rentButton setSelected:NO];
        
    }else{
        [self.delegate selectTabAtIndex:1];
        [self.meetButton setSelected:NO];
        [self.rentButton setSelected:YES];
    }

}

@end
