//
//  URTabBarVC.m
//  uRide
//
//  Created by Leo Lashkevich on 7/5/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import "URTabBarVC.h"
#import "URTabView.h"

#import "URRentServicesTableViewController.h"

@interface URTabBarVC () <URTabViewDelegate>

@end

@implementation URTabBarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    float quotient = 10.2;
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height/quotient;
    CGFloat y = self.view.frame.size.height - height;
    
    CGRect rect = CGRectMake(0, y, width, height);
    
    URTabView *tBar = [URTabView createWithFrame:rect];
    
    [self.view addSubview:tBar];
    
    tBar.delegate = self;
    
    UINavigationController* nav = [self.viewControllers lastObject];
    URRentServicesTableViewController* vc = [nav.viewControllers firstObject];
    vc.city = self.city;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)selectTabAtIndex:(NSInteger)index{
    [self setSelectedIndex:index];
}


@end
