//
//  URAccessToken.h
//  uRide
//
//  Created by Anton Ivashyna on 7/14/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URAccessToken : NSObject

@property (strong, nonatomic) NSString* token;
@property (strong, nonatomic) NSDate*   expirationDate;
@property (strong, nonatomic) NSString* userID;

@end
