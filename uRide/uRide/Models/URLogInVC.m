//
//  URLogInVC.m
//  uRide
//
//  Created by Leo Lashkevich on 6/24/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//
//
// TODO - 1. add animations in "howToDo" button

#import "URLogInVC.h"
#import "URTabBarVC.h"

#import "URMainManager.h"

#import "URUser.h"
#import "TipPageViewController.h"
#import "WYPopoverController.h"

@implementation URLogInVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.vkontakteButton.alpha = 0.f;
    self.facebookButton.alpha = 0.f;
    
    [self checkUserToBeLoggedIn];
                
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES];
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [UIView animateWithDuration:2.0f
                     animations:^{
                         self.vkontakteButton.alpha = 1.0f;
                         self.facebookButton.alpha = 0.2f;
                     }
                     completion:^(BOOL finished) {
                         nil;
                     }];
    
}

- (void) checkUserToBeLoggedIn {
    
    BOOL isLoggedIn = [[NSUserDefaults standardUserDefaults] boolForKey:@"isLoggedIn"];
    
    if (isLoggedIn == YES) {
        [self configureViewLikeUserLoggedIn];
    } else {
        [self configureViewLikeThereIsNoLoggedInUser];
    }
    
}

- (BOOL) VKTokenAvailable {
    
    NSDate* tokenExpirationDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"tokenExpirationDate"];
    NSDate* nowDate = [NSDate date];
    
    return [tokenExpirationDate compare:nowDate] == NSOrderedDescending;
}

#pragma mark - Actions

- (IBAction)vkontakteButtonAction:(UIButton *)sender {
    
    [self makeScaleAnimationForButton:sender];
    [self makeInteractionsDelay:1.0f];
    
    [[URMainManager sharedManager] autorizeUser:^(URUser *user) {
        
        URTabBarVC* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"URTabBarVC"];
        [self.navigationController pushViewController:vc animated:YES];
        
    }];
    
}

- (IBAction)facebookButtonAction:(UIButton *)sender {
    
}

- (IBAction)howToUseButtonAction:(UIButton *)sender {
    TipPageViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TipPageViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController presentViewController:vc animated:YES completion:nil];
    
}

- (IBAction)nextAction:(UIButton *)sender {                                         // this button appears, when user is logged in
    
    BOOL tokenExpirationDateAvailable = [self VKTokenAvailable];
    
    if (tokenExpirationDateAvailable == NO) {                                       // check vk token to be available to use (max date to use - 24 hrs)
        
        [[URMainManager sharedManager] autorizeUser:^(URUser *user) {
            
            URTabBarVC* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"URTabBarVC"];
            [self.navigationController pushViewController:vc animated:YES];
        }];
        
    } else {
        
        URTabBarVC* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"URTabBarVC"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

- (IBAction)logOutAction:(UIButton *)sender {
    
    [[URMainManager sharedManager] logOutUserWithExitBlock:^(NSString *success) {
    }];
    
    [self configureViewLikeThereIsNoLoggedInUser];
}

#pragma mark - View configures and animations

- (void) configureViewLikeUserLoggedIn {
    
    NSString* userFirstName = [[NSUserDefaults standardUserDefaults] objectForKey:@"firstName"];
    NSString* userLastName  = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastName"];

    self.facebookButton.hidden  = YES;
    self.vkontakteButton.hidden = YES;
    self.howToUseButton.hidden  = YES;
    self.soonLabel.hidden       = YES;
    self.enterLabel.text        = @"Привет";
    self.userNameLabel.hidden   = NO;
    self.userNameLabel.text     = [NSString stringWithFormat:@"%@ %@", userFirstName, userLastName];
    self.nextButton.hidden      = NO;
    self.logOutButton.hidden    = NO;
}

- (void) configureViewLikeThereIsNoLoggedInUser {
    
    self.facebookButton.hidden  = NO;
    self.vkontakteButton.hidden = NO;
    self.howToUseButton.hidden  = NO;
    self.soonLabel.hidden       = NO;
    self.enterLabel.text        = @"Войти:";
    self.userNameLabel.hidden   = YES;
    self.nextButton.hidden      = YES;
    self.logOutButton.hidden    = YES;
    
    [UIView animateWithDuration:2.0f
                     animations:^{
                         self.vkontakteButton.alpha = 1.0f;
                         self.facebookButton.alpha = 0.2f;
                     }
                     completion:^(BOOL finished) {
                         nil;
                     }];
    
}



- (void) makeScaleAnimationForButton: (UIButton*) button {
    
    [UIView animateWithDuration:0.2f
                          delay:0
                        options:UIViewAnimationOptionAutoreverse
                     animations:^{
                         button.transform = CGAffineTransformMakeScale(1.1f, 1.1f);
                     }
                     completion:^(BOOL finished) {
                         button.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
                     }];
    
}

- (void) makeInteractionsDelay:(CGFloat) delay {                                    // not to allow user a lot of touches while logging in

    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,
                                 (int64_t)(delay * NSEC_PER_SEC)),
                   dispatch_get_main_queue(), ^{
                       [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                   });
    
}

@end
