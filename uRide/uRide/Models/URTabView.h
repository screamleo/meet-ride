//
//  URTabView.h
//  uRide
//
//  Created by Leo Lashkevich on 7/5/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol URTabViewDelegate

-(void)selectTabAtIndex:(NSInteger)index;

@end

@interface URTabView : UIView

+(URTabView*)createWithFrame:(CGRect)frame;

@property (nonatomic, weak) IBOutlet UIButton* meetButton;
@property (nonatomic, weak) IBOutlet UIButton* rentButton;

@property (nonatomic, weak) id delegate;

@end
