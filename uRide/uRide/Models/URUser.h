//
//  URUser.h
//  uRide
//
//  Created by Anton Ivashyna on 7/14/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface URUser : NSObject

@property (strong, nonatomic) NSString*  lastName;
@property (strong, nonatomic) NSString*  firstName;
@property (strong, nonatomic) NSString*  phoneNumber;
@property (strong, nonatomic) NSString*  scooter;
@property (strong, nonatomic) NSString*  city;

@property (strong, nonatomic) UIImage*   photo_200;

@property (strong, nonatomic) NSString*  userID;


- (id) initWithServerResponse:(NSDictionary*) responseObject;
+ (URUser*) currentUser;

@end
