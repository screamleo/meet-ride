//
//  URRentServiceTableViewController.h
//  uRide
//
//  Created by Anton Ivashyna on 7/6/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "URRentService.h"

@interface URRentServiceTableViewController : UITableViewController <UICollectionViewDataSource,
                                                                     UICollectionViewDelegate>

@property (strong, nonatomic) URRentService* rentService;

@property (strong, nonatomic) NSArray* scooterPhotosData;
@property (assign, nonatomic) NSString* distanceFromUser;

@end
