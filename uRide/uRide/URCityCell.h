//
//  URCityTableViewCell.h
//  uRide
//
//  Created by Anton Ivashyna on 7/3/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface URCityCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *cityPictureImageView;
@property (weak, nonatomic) IBOutlet UILabel *cityNameLabel;



@end
