//
//  URNewMeetTableViewController.h
//  uRide
//
//  Created by Anton Ivashyna on 7/17/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface URNewMeetTableViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UIImageView *greenCircleImageVIew;     // when there is no photo for meet we have to fide label '+ photo' and make this circle dashed
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;

@property (weak, nonatomic) IBOutlet UIButton *addPhotoButton;             

@property (weak, nonatomic) IBOutlet UITextField *meetNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *meetTimeTextField;
@property (weak, nonatomic) IBOutlet UITextField *meetDateTextField;


- (IBAction)actionOpenMap:(id)sender;
- (IBAction)actionOpenTimePopover:(id)sender;
- (IBAction)actionOpenDatePopover:(id)sender;
- (IBAction)actionSaveNewMeet:(id)sender;
- (IBAction)actionAddPhoto:(id)sender;

@end
