//
//  main.m
//  uRide
//
//  Created by Leo Lashkevich on 6/23/15.
//  Copyright (c) 2015 Worldwide Developers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "URAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([URAppDelegate class]));
    }
}
